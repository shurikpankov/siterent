﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SiteRent.Startup))]
namespace SiteRent
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
