﻿using SiteRent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;


namespace SiteRent.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public async Task<ActionResult> Index()
        {
            Employees currUser = null;

            var houses = db.Houses.Include(h => h.Employee);

            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            DateTime now = DateTime.Now;
            int month = now.Month;
            int year = now.Year;
            string monthstr = "МЕСЯЦА";
            switch (month)
            {
                case 1: monthstr = "ЯНВАРЯ"; break;
                case 2: monthstr = "ФЕВРАЛЯ"; break;
                case 3: monthstr = "МАРТА"; break;
                case 4: monthstr = "АПРЕЛЯ"; break;
                case 5: monthstr = "МАЯ"; break;
                case 6: monthstr = "ИЮНЯ"; break;
                case 7: monthstr = "ИЮЛЯ"; break;
                case 8: monthstr = "АВГУСТА"; break;
                case 9: monthstr = "СЕНТЯБРЯ"; break;
                case 10: monthstr = "ОКТЯБРЯ"; break;
                case 11: monthstr = "НОЯБРЯ"; break;
                case 12: monthstr = "ДЕКАБРЯ"; break;
                default: break;
            }
            ViewBag.MonthStr = monthstr;

            #region Переговорщик_Сопровождающий
            if (currUser != null && (currUser.Role == "Переговорщик" || currUser.Role == "Сопровождающий"))
            {
                houses = houses.Where(x => x.Employee.Email == User.Identity.Name);
                ViewBag.TotalHouses = await houses.CountAsync();
                ViewBag.InNegotiations = await houses.Where(x => x.Status == "Согласование с УК").CountAsync();
                ViewBag.UKCount = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") != "").Select(x => x.UK).Distinct().CountAsync();
                ViewBag.UKNotSet = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") == "").CountAsync();
                ViewBag.StateAgreed = await houses.Where(x => x.FactAgreed != null).CountAsync();
                ViewBag.StateNegotive = await houses.Where(x => x.Status == "Не согласован с УК").CountAsync();


                var plans = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Personal" && x.EmployeeId == currUser.Id && x.Period == month && x.Year == year);
                if (await plans.CountAsync() > 0)
                {
                    ViewBag.CurrentPlan = await plans.Select(x => x.Value).SumAsync();
                }
                else
                {
                    ViewBag.CurrentPlan = null;
                }

                var factStats = houses.Where(x => x.FactDate.Value.Year == year)
                    .GroupBy(g => g.FactDate.Value.Month)
                    .Select(g => new { Month = g.Key, FactCount = g.Count() })
                    .OrderBy(n => n.Month);

                ViewBag.FactStats = await factStats.ToDictionaryAsync(r => r.Month, r => r.FactCount);

                //fact
                var factHouses = houses.Where(x => x.FactDate.Value.Month == month);
                ViewBag.Fact = await factHouses.CountAsync();
                ViewBag.MotivationFact = await factHouses.Where(x => x.IsPersonal).CountAsync();


                //old data


                List<int> _plan = new List<int>();
                List<int> _UK = new List<int>();
                List<int> _fact = new List<int>();
                List<int> _result = new List<int>();
                List<int> _MotivationPlan = new List<int>();
                List<int> _MotivationFact = new List<int>();
                List<int> _MotivationResult = new List<int>();     

                var emplHousesPlan = houses.Where(x => x.FactResponsible == currUser.Id && x.IsPlanned == true && x.PlanApproval.Value.Year == 2018);
                var emplHousesFact = emplHousesPlan.Where(x => x.IsApproved == true && x.FactDate.Value.Year == 2018);
                var emplMotivationPlan = emplHousesPlan.Where(x => x.IsPersonal == true);
                var emplMotivationFact = emplHousesFact.Where(x => x.IsPersonal == true);
                _plan.Add(await emplHousesPlan.CountAsync());
                _fact.Add(await emplHousesFact.CountAsync());
                _UK.Add(await emplHousesPlan.Select(x => x.UK).Distinct().CountAsync());
                _MotivationPlan.Add(await emplMotivationPlan.CountAsync());
                _MotivationFact.Add(await emplMotivationFact.CountAsync());

                List<int> _master = new List<int>();
                var emplMasterPlanPersonal = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Personal" && x.EmployeeId == currUser.Id);
                var emplMasterPlanPersonalCount = await emplMasterPlanPersonal.CountAsync();
                _master.Add((emplMasterPlanPersonalCount == 0) ? -1 : Convert.ToInt32(await emplMasterPlanPersonal.Select(x => x.Value).SumAsync()));

                for (int m = 1; m <= 12; m++)
                {
                    var mEmplHousesPlan = emplHousesPlan.Where(x => x.PlanApproval.Value.Month == m && x.PlanApproval.Value.Year == 2018);
                    var mEmplHousesFact = emplHousesFact.Where(x => x.FactDate.Value.Month == m && x.FactDate.Value.Year == 2018);
                    var mEmplMotivationPlan = emplMotivationPlan.Where(x => x.PlanApproval.Value.Month == m && x.PlanApproval.Value.Year == 2018);
                    var mEmplMotivationFact = emplMotivationFact.Where(x => x.FactDate.Value.Month == m && x.FactDate.Value.Year == 2018);
                    _plan.Add(await mEmplHousesPlan.CountAsync());
                    _fact.Add(await mEmplHousesFact.CountAsync());
                    _UK.Add(await mEmplHousesPlan.Select(x => x.UK).Distinct().CountAsync());
                    _MotivationPlan.Add(await mEmplMotivationPlan.CountAsync());
                    _MotivationFact.Add(await mEmplMotivationFact.CountAsync());

                    var mMasterPlanPersonal = emplMasterPlanPersonal.Where(x => x.Period == m && x.Year == 2018);
                    var mMasterPlanPersonalCount = await mMasterPlanPersonal.CountAsync();
                    _master.Add((mMasterPlanPersonalCount == 0) ? -1 : Convert.ToInt32(await mMasterPlanPersonal.Select(x => x.Value).SumAsync()));
                }
                
                for (int i = 0; i < _fact.Count; i++)
                {
                    if (_plan[i] == 0)
                    {
                        _result.Add(-1);
                    }
                    else
                    {
                        double dPlan = Convert.ToDouble(_plan[i]);
                        double dFact = Convert.ToDouble(_fact[i]);
                        double dResult = dFact / dPlan;
                        _result.Add(Convert.ToInt32((dResult * 100)));                                                
                    }
                    if (_MotivationPlan[i] == 0)
                    {
                        if (i <= 2 && DateTime.Now.Year == 2018)
                        {
                            _MotivationResult.Add(-1);
                        }
                        else
                        {
                            _MotivationResult.Add(100);
                        }
                    }
                    else
                    {
                        double mPlan = Convert.ToDouble(_MotivationPlan[i]);
                        double mFact = Convert.ToDouble(_MotivationFact[i]);
                        double mResult = mFact / mPlan;
                        if (i == 0)
                        {
                            _MotivationResult.Add(Convert.ToInt32((mResult * 100)));
                        }
                        else
                        {
                            if (mResult < 0.70)
                            {
                                _MotivationResult.Add(0);
                            }
                            else
                            {
                                if (mResult >= 0.70 && mResult < 1.00)
                                {
                                    _MotivationResult.Add(75);
                                }
                                else
                                {
                                    if (mResult >= 1.00 && mResult < 1.20)
                                    {
                                        _MotivationResult.Add(100);
                                    }
                                    else
                                    {
                                        if (mResult >= 1.20 && mResult < 1.50)
                                        {
                                            _MotivationResult.Add(120);
                                        }
                                        else
                                        {
                                            if (mResult >= 1.5)
                                            {
                                                _MotivationResult.Add(150);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                ViewBag.EmplPlan = _plan;
                ViewBag.EmplUK = _UK;
                ViewBag.EmplFact = _fact;                
                ViewBag.EmplResult = _result;
                ViewBag.EmplMasterPlan = _master;
                ViewBag.EmplMotivationPlan = _MotivationPlan;
                ViewBag.EmplMotivationFact = _MotivationFact;
                ViewBag.EmplMotivationResult = _MotivationResult;

                var firstUK = await houses.Where(x => x.FactResponsible == currUser.Id && x.IsPlanned == true && x.IsApproved != true).GroupBy(g => g.UK).Select(g => new { UK = g.Key, UKCount = g.Count(), Date = g.FirstOrDefault().PlanApproval }).Take(10).OrderBy(n => n.Date).ToDictionaryAsync(r => r.UK, r => Tuple.Create(r.Date, r.UKCount));

                ViewBag.EmplFirstNegotiations = firstUK;
                //var firstUK = await houses.Where(x => x.FactResponsible == currUser.Id && x.IsPlanned == true && x.IsApproved != true).OrderBy(x => x.PlanApproval).GroupBy(g => g.UK).Select(g => new { UK = g.Key, UKCount = g.Count() }).Distinct().Take(10).ToListAsync();
            }

            #endregion Переговорщик_Сопровождающий

            #region Руководитель

            if (currUser != null && currUser.Role == "Руководитель")
            {
                List<String> userTerritories = currUser.Filials.Replace(", ", "!").Split('!').ToList();

                houses = houses.Where(x => userTerritories.Contains(x.Filial));
                ViewBag.TotalHouses = await houses.CountAsync();
                ViewBag.InNegotiations = await houses.Where(x => x.Status == "Согласование с УК").CountAsync();
                ViewBag.UKCount = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") != "").Select(x => x.UK).Distinct().CountAsync();
                ViewBag.UKNotSet = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") == "").CountAsync();
                ViewBag.StateAgreed = await houses.Where(x => x.FactAgreed != null).CountAsync();
                ViewBag.StateNegotive = await houses.Where(x => x.Status == "Не согласован с УК").CountAsync();

                var plans = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Filial" && userTerritories.Contains(x.Filial) && x.Period == month && x.Year == year);
                if (await plans.CountAsync() > 0)
                {
                    ViewBag.CurrentPlan = await plans.Select(x => x.Value).SumAsync();
                }
                else
                {
                    ViewBag.CurrentPlan = null;
                }

                //fact
                ViewBag.Fact = await houses.Where(x => x.FactDate.Value.Month == month).CountAsync();

                List<int> _plan = new List<int>();
                List<int> _fact = new List<int>();
                List<int> _result = new List<int>();
                List<int> _master = new List<int>();
                List<int> _UK = new List<int>();
                List<int> _Empl = new List<int>();

                var emplHousesPlan = houses.Where(x => x.IsPlanned == true && x.PlanApproval.Value.Year == 2018);
                var emplHousesFact = emplHousesPlan.Where(x => x.IsApproved == true && x.FactDate.Value.Year == 2018);
                _plan.Add(await emplHousesPlan.CountAsync());
                _fact.Add(await emplHousesFact.CountAsync());
                _UK.Add(await emplHousesPlan.Select(x => x.UK).Distinct().CountAsync());
                _Empl.Add(await emplHousesPlan.Select(x => x.FactResponsible).Distinct().CountAsync());

                var emplMasterPlanPersonal = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Filial" && userTerritories.Contains(x.Filial));
                var emplMasterPlanPersonalCount = await emplMasterPlanPersonal.CountAsync();
                _master.Add((emplMasterPlanPersonalCount == 0) ? -1 : Convert.ToInt32(await emplMasterPlanPersonal.Select(x => x.Value).SumAsync()));

                for (int m = 1; m <= 12; m++)
                {
                    var mEmplHousesPlan = emplHousesPlan.Where(x => x.PlanApproval.Value.Month == m && x.PlanApproval.Value.Year == 2018);
                    var mEmplHousesFact = emplHousesFact.Where(x => x.FactDate.Value.Month == m && x.FactDate.Value.Year == 2018);
                    _plan.Add(await mEmplHousesPlan.CountAsync());
                    _fact.Add(await mEmplHousesFact.CountAsync());
                    _UK.Add(await mEmplHousesPlan.Select(x => x.UK).Distinct().CountAsync());
                    _Empl.Add(await mEmplHousesPlan.Select(x => x.FactResponsible).Distinct().CountAsync());

                    var mMasterPlanPersonal = emplMasterPlanPersonal.Where(x => x.Period == m && x.Year == 2018);
                    var mMasterPlanPersonalCount = await mMasterPlanPersonal.CountAsync();
                    _master.Add((mMasterPlanPersonalCount == 0) ? -1 : Convert.ToInt32(await mMasterPlanPersonal.Select(x => x.Value).SumAsync()));
                }

                for (int i = 0; i < _fact.Count; i++)
                {
                    if (_plan[i] == 0)
                    {
                        _result.Add(100);
                    }
                    else
                    {
                        double dPlan = Convert.ToDouble(_master[i]);
                        double dFact = Convert.ToDouble(_fact[i]);
                        double dResult = dFact / dPlan;
                        if (i == 0)
                        {
                            _result.Add(Convert.ToInt32((dResult * 100)));
                        }
                        else
                        {
                            if (dResult < 0.70)
                            {
                                _result.Add(0);
                            }
                            else
                            {
                                if (dResult >= 0.70 && dResult < 1.00)
                                {
                                    _result.Add(70);
                                }
                                else
                                {
                                    if (dResult >= 1.00 && dResult < 1.20)
                                    {
                                        _result.Add(100);
                                    }
                                    else
                                    {
                                        if (dResult >= 1.20 && dResult < 1.50)
                                        {
                                            _result.Add(120);
                                        }
                                        else
                                        {
                                            if (dResult >= 1.5)
                                            {
                                                _result.Add(150);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                ViewBag.EmplPlan = _plan;
                ViewBag.EmplFact = _fact;
                ViewBag.EmplResult = _result;
                ViewBag.EmplMasterPlan = _master;
                ViewBag.EmplUK = _UK;
                ViewBag.EmplCount = _Empl;

                var bigUK = await houses.Where(x => x.Region == currUser.Regions && x.IsPlanned == true && x.IsApproved != true).GroupBy(g => new { g.Filial, g.UK }).Select(g => new { UK = g.Key.UK, UKCount = g.Count(), Fil = g.FirstOrDefault().Filial }).OrderByDescending(n => n.UKCount).Take(10).ToDictionaryAsync(r => r.UK, r => Tuple.Create(r.Fil, r.UKCount));

                ViewBag.BigUK = bigUK;
            }

            #endregion Руководитель

            #region Ассистент
            if (currUser != null && (currUser.Role == "Ассистент"))
            {
                List<String> userTerritories = currUser.Filials.Replace(", ", "!").Split('!').ToList();

                houses = houses.Where(x => userTerritories.Contains(x.Filial));
                ViewBag.TotalHouses = await houses.CountAsync();
                ViewBag.InNegotiations = await houses.Where(x => x.Status == "Согласование с УК").CountAsync();
                ViewBag.UKCount = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") != "").Select(x => x.UK).Distinct().CountAsync();
                ViewBag.UKNotSet = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") == "").CountAsync();
                ViewBag.StateAgreed = await houses.Where(x => x.FactAgreed != null).CountAsync();
                ViewBag.StateNegotive = await houses.Where(x => x.Status == "Не согласован с УК").CountAsync();

                var plans = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Filial" && userTerritories.Contains(x.Filial) && x.Period == month && x.Year == year);
                if (await plans.CountAsync() > 0)
                {
                    ViewBag.CurrentPlan = await plans.Select(x => x.Value).SumAsync();
                }
                else
                {
                    ViewBag.CurrentPlan = null;
                }

                //fact
                ViewBag.Fact = await houses.Where(x => x.FactDate.Value.Month == month).CountAsync();

            }
            #endregion Ассистент

            #region HQ
            if (currUser != null && (currUser.Role == "Штаб-квартира"))
            {
                ViewBag.TotalHouses = await houses.CountAsync();
                ViewBag.InNegotiations = await houses.Where(x => x.Status == "Согласование с УК").CountAsync();
                ViewBag.UKCount = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") != "").Select(x => x.UK).Distinct().CountAsync();
                ViewBag.UKNotSet = await houses.Where(x => x.Status == "Согласование с УК" && x.UK.Replace("-", "").Replace(" ", "") == "").CountAsync();
                ViewBag.StateAgreed = await houses.Where(x => x.FactAgreed != null).CountAsync();
                ViewBag.StateNegotive = await houses.Where(x => x.Status == "Не согласован с УК").CountAsync();


                var plans = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Total" && x.Period == month && x.Year == year);
                if (await plans.CountAsync() > 0)
                {
                    ViewBag.CurrentPlan = await plans.Select(x => x.Value).SumAsync();
                }
                else
                {
                    ViewBag.CurrentPlan = null;
                }

                //fact
                ViewBag.Fact = await houses.Where(x => x.FactDate.Value.Month == month).CountAsync();
            }
            #endregion HQ


            /*ViewBag.UserTerritories = currUser.Filials.Replace(", ", "!").Split('!').ToList();
            List<int> terrHCount = new List<int>();
            foreach (string ter in ViewBag.UserTerritories)
            {
                terrHCount.Add(db.Houses.Where(x => x.Filial == ter).Count());
            }
            ViewBag.TerritoryHouses = terrHCount;
            */

            var states = db.DatabaseStates.OrderByDescending(s => s.Id).Take(6);
            ViewBag.DatabaseStates = await states.ToDictionaryAsync(s => s.State, s => s.SourceDate);


            return View();
        }

        

        [HttpGet]
        public async Task<JsonResult> GetFactData()
        {
            List<int> Stats = new List<int>();
            Dictionary<int, int> planStats = new Dictionary<int, int>();

            Employees currUser = null;

            var houses = db.Houses.Include(h => h.Employee);

            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            DateTime now = DateTime.Now;
            int month = now.Month;
            int year = now.Year;

            //переговорщик_сопровождающий
            if (currUser != null && (currUser.Role == "Переговорщик" || currUser.Role == "Сопровождающий"))
            {
                houses = houses.Where(x => x.Employee.Email == User.Identity.Name);

                planStats = await db.MasterPlans.Where(p => p.Subject == "RO" && p.Type == "Personal" && p.Year == year && p.EmployeeId == currUser.Id)
                    .GroupBy(g => g.Period)
                    .Select(g => new { Month = g.Key, PlanCount = g.Sum(s => (int)s.Value) })
                    .OrderBy(n => n.Month).ToDictionaryAsync(r => r.Month, r => r.PlanCount);
            }

            //руководитель
            if (currUser != null && currUser.Role == "Руководитель")
            {
                List<String> userTerritories = currUser.Filials.Replace(", ", "!").Split('!').ToList();
                houses = houses.Where(x => userTerritories.Contains(x.Filial));

                planStats = await db.MasterPlans.Where(p => p.Subject == "RO" && p.Type == "Filial" && p.Year == year && userTerritories.Contains(p.Filial))
                    .GroupBy(g => g.Period)
                    .Select(g => new { Month = g.Key, PlanCount = g.Sum(s => (int)s.Value) })
                    .OrderBy(n => n.Month).ToDictionaryAsync(r => r.Month, r => r.PlanCount);

            }

            //ассистент
            if (currUser != null && (currUser.Role == "Ассистент"))
            {
                List<String> userTerritories = currUser.Filials.Replace(", ", "!").Split('!').ToList();
                houses = houses.Where(x => userTerritories.Contains(x.Filial));

                planStats = await db.MasterPlans.Where(p => p.Subject == "RO" && p.Type == "Filial" && p.Year == year && userTerritories.Contains(p.Filial))
                    .GroupBy(g => g.Period)
                    .Select(g => new { Month = g.Key, PlanCount = g.Sum(s => (int)s.Value) })
                    .OrderBy(n => n.Month).ToDictionaryAsync(r => r.Month, r => r.PlanCount);
            }

            //штаб-квартира
            if (currUser != null && (currUser.Role == "Штаб-квартира"))
            {
                planStats = await db.MasterPlans.Where(p => p.Subject == "RO" && p.Type == "Total" && p.Year == year)
                    .GroupBy(g => g.Period)
                    .Select(g => new { Month = g.Key, PlanCount = g.Sum(s => (int)s.Value) })
                    .OrderBy(n => n.Month).ToDictionaryAsync(r => r.Month, r => r.PlanCount);
            }

            for (int i = 1; i <= 12; i++)
            {
                if (planStats.Keys.Contains(i))
                {
                    Stats.Add(planStats[i]);
                }
                else
                {
                    Stats.Add(0);
                }
            }

            //fact
            var factStats = await houses.Where(x => x.FactDate.Value.Year == year)
                .GroupBy(g => g.FactDate.Value.Month)
                .Select(g => new { Month = g.Key, FactCount = g.Count() })
                .OrderBy(n => n.Month).ToDictionaryAsync(r => r.Month, r => r.FactCount);

            
            for (int i = 1; i <= 12; i++)
            {
                if (factStats.Keys.Contains(i))
                {
                    Stats.Add(factStats[i]);
                }
                else
                {
                    Stats.Add(0);
                }
            }

            /*
            List<int> _plan = new List<int>();
            List<int> _fact = new List<int>();
            List<int> _master = new List<int>();
            List<int> _dostup = new List<int>();

            var houses = db.Houses.Where(x => x.RoYear == 2018 && x.IsPlanned == true);
            var master = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Filial");
            var nriHouses = db.Houses.Where(x => x.RoYear == 2018);

            for (int m = 1; m <= 12; m++)
            {
                var mEmplHousesPlan = houses.Where(x => x.PlanApproval.Value.Month == m && x.PlanApproval.Value.Year == 2018);
                var mEmplHousesFact = houses.Where(x => x.FactDate.Value.Month == m && x.FactDate.Value.Year == 2018);
                _plan.Add(await mEmplHousesPlan.CountAsync());
                _fact.Add(await mEmplHousesFact.CountAsync());
                if (m == 1)
                {
                    _dostup.Add(await nriHouses.Where(x => x.FactAgreed.Value.Month <= m && x.FactAgreed.Value.Year <= DateTime.Now.Year).CountAsync());
                }
                else
                {
                    _dostup.Add(await nriHouses.Where(x => x.FactAgreed.Value.Month == m && x.FactAgreed.Value.Year == DateTime.Now.Year).CountAsync());
                }
                var mMasterPlanPersonal = master.Where(x => x.Period == m && x.Year == 2018);
                var emplMasterPlanPersonalCount = await mMasterPlanPersonal.CountAsync();
                _master.Add((emplMasterPlanPersonalCount == 0) ? -1 : Convert.ToInt32(await mMasterPlanPersonal.Select(x => x.Value).SumAsync()));
            }

            List<int> result = new List<int>();
            //result.AddRange(_plan);
            //result.AddRange(_fact);
            //result.AddRange(_master);
            result.AddRange(_dostup);
            

            int[] tmp = new int[] { 0, 0, 4, 8, 8, 24, 8, 8, 12, 8, 8, 6, 4, 8, 15, 2, 23, 42, 4, 8, 15, 9, 23, 42 };
            */

            return Json(Stats.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "О приложении";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Контакты";

            return View();
        }
    }
}