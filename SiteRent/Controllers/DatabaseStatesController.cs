﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SiteRent.Models;

namespace SiteRent.Controllers
{
    public class DatabaseStatesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DatabaseStates
        public async Task<ActionResult> Index()
        {
            return View(await db.DatabaseStates.ToListAsync());
        }

        // GET: DatabaseStates/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatabaseStates databaseStates = await db.DatabaseStates.FindAsync(id);
            if (databaseStates == null)
            {
                return HttpNotFound();
            }
            return View(databaseStates);
        }

        // GET: DatabaseStates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DatabaseStates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,State,Progress,Success,SourceDate")] DatabaseStates databaseStates)
        {
            if (ModelState.IsValid)
            {
                db.DatabaseStates.Add(databaseStates);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(databaseStates);
        }

        // GET: DatabaseStates/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatabaseStates databaseStates = await db.DatabaseStates.FindAsync(id);
            if (databaseStates == null)
            {
                return HttpNotFound();
            }
            return View(databaseStates);
        }

        // POST: DatabaseStates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,State,Progress,Success,SourceDate")] DatabaseStates databaseStates)
        {
            if (ModelState.IsValid)
            {
                db.Entry(databaseStates).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(databaseStates);
        }

        // GET: DatabaseStates/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatabaseStates databaseStates = await db.DatabaseStates.FindAsync(id);
            if (databaseStates == null)
            {
                return HttpNotFound();
            }
            return View(databaseStates);
        }

        // POST: DatabaseStates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DatabaseStates databaseStates = await db.DatabaseStates.FindAsync(id);
            db.DatabaseStates.Remove(databaseStates);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
