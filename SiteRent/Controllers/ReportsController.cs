﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SiteRent.Models;

namespace SiteRent.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Reports
        public async Task<ActionResult> Index()
        {
            return View();
            //return View(await db.Reports.ToListAsync());
        }

        public async Task<ActionResult> Knowledge()
        {
            return View();
            //return View(await db.Reports.ToListAsync());
        }

        public async Task<ActionResult> LearningBusiness()
        {
            return View();
            //return View(await db.Reports.ToListAsync());
        }

        // GET: Reports/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reports reports = await db.Reports.FindAsync(id);
            if (reports == null)
            {
                return HttpNotFound();
            }
            return View(reports);
        }

        // GET: Reports/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Subject,Type,Year,Period,Region,Filial,EmployeeId,Value,Date")] Reports reports)
        {
            if (ModelState.IsValid)
            {
                db.Reports.Add(reports);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(reports);
        }

        // GET: Reports/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reports reports = await db.Reports.FindAsync(id);
            if (reports == null)
            {
                return HttpNotFound();
            }
            return View(reports);
        }

        // POST: Reports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Subject,Type,Year,Period,Region,Filial,EmployeeId,Value,Date")] Reports reports)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reports).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(reports);
        }

        // GET: Reports/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reports reports = await db.Reports.FindAsync(id);
            if (reports == null)
            {
                return HttpNotFound();
            }
            return View(reports);
        }

        // POST: Reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Reports reports = await db.Reports.FindAsync(id);
            db.Reports.Remove(reports);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public async Task<JsonResult> GetROReportData()
        {
            List<int> _plan = new List<int>();
            List<int> _fact = new List<int>();
            List<int> _master= new List<int>();
            List<int> _dostup = new List<int>();

            var houses = db.Houses.Where(x => x.RoYear == 2018 && x.IsPlanned == true);
            var master = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Filial");
            var nriHouses = db.Houses.Where(x => x.RoYear == 2018);

            for (int m = 1; m <= 12; m++)
            {
                var mEmplHousesPlan = houses.Where(x => x.PlanApproval.Value.Month == m && x.PlanApproval.Value.Year == 2018);
                var mEmplHousesFact = houses.Where(x => x.FactDate.Value.Month == m && x.FactDate.Value.Year == 2018);
                _plan.Add(await mEmplHousesPlan.CountAsync());
                _fact.Add(await mEmplHousesFact.CountAsync());
                if (m == 1)
                {
                    _dostup.Add(await nriHouses.Where(x => x.FactAgreed.Value.Month <= m && x.FactAgreed.Value.Year <= DateTime.Now.Year).CountAsync());
                }
                else
                {
                    _dostup.Add(await nriHouses.Where(x => x.FactAgreed.Value.Month == m && x.FactAgreed.Value.Year == DateTime.Now.Year).CountAsync());
                }
                var mMasterPlanPersonal = master.Where(x => x.Period == m && x.Year == 2018);
                var emplMasterPlanPersonalCount = await mMasterPlanPersonal.CountAsync();
                _master.Add((emplMasterPlanPersonalCount == 0) ? -1 : Convert.ToInt32(await mMasterPlanPersonal.Select(x => x.Value).SumAsync()));
            }

            List<int> result = new List<int>();
            result.AddRange(_plan);
            result.AddRange(_fact);
            result.AddRange(_master);
            result.AddRange(_dostup);
            return Json(result.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<ContentResult> GetTestGeoJson()
        {           
            var houses = await db.Houses.Where(x => x.Latitude != null && x.Longitude != null && x.FactDate.HasValue && x.FactDate.Value.Year == DateTime.Now.Year).ToListAsync();
            return Content(GetJson(houses), "application/json");
        }

        public static string GetJson(List<Houses> houses)
        {
            string result = @"{
  &type&: &FeatureCollection&,
  &features&: [
    ";
            for(int i = 0; i < houses.Count; i++)
            {
                result += @"{
      &type&: &Feature&,
      &geometry&: {
        &type&: &Point&,
        &coordinates&: [
          "+houses[i].Latitude.ToString().Replace(',', '.')+@",
          "+houses[i].Longitude.ToString().Replace(',', '.') + @"
        ]
      },
      &properties&: {}
    }";
                if (i != houses.Count - 1)
                {
                    result += @",
    ";
                }
            }

            result += @"
  ]
}";

            return result.Replace('&', '"');
        }

        public async Task<ActionResult> CurrentEmployees()
        {
            Employees currUser = null;
            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            if (currUser != null && (currUser.Role == "Штаб-квартира" || currUser.Role == "Руководитель"))
            {
                List<String> userTerritories = currUser.Regions.Replace(", ", "!").Split('!').ToList();
                var employees = await db.Employees.Where(x => userTerritories.Contains(x.Regions) && !String.IsNullOrEmpty(x.Role)).OrderBy(x => x.Regions).ThenBy(x => x.Filials).ThenBy(x => x.Surname).ToListAsync();

                List<int> _totalHouses = new List<int>();
                List<int> _totalUK = new List<int>();
                List<int> _currPlan = new List<int>();
                List<int> _currFact = new List<int>();
                List<int> _currProgress = new List<int>();


                foreach (var empl in employees)
                {
                    var houses = db.Houses.Where(x => x.EmployeeId == empl.Id && x.RoYear == DateTime.Now.Year);
                    int th = await houses.CountAsync();
                    _totalHouses.Add(th);
                    _totalUK.Add(await houses.Select(x => x.UK).Distinct().CountAsync());
                    var personalhouses = db.Houses.Where(x => x.FactResponsible == empl.Id && x.IsPlanned == true && x.RoYear == DateTime.Now.Year);
                    _currPlan.Add(await personalhouses.CountAsync());
                    int cf = await personalhouses.Where(x => x.FactDate != null).CountAsync();
                    _currFact.Add(cf);
                    double progr = (th > 0) ? Convert.ToDouble(cf) * 100.0 / Convert.ToDouble(th) : 100.0;
                    _currProgress.Add(Convert.ToInt32(progr));
                }
                //всего домов в NRI
                //всего УК в NRI
                //план текущего года
                //факт текущего года
                //прогресс %
                ViewBag.HousesInNri = _totalHouses;
                ViewBag.UKInNri = _totalUK;
                ViewBag.Plans = _currPlan;
                ViewBag.Facts = _currFact;
                ViewBag.Progress = _currProgress;

                return View(employees);
            }
            else
            {
                return View();
            }            
        }

        public async Task<ActionResult> EmployeesPlans()
        {
            Employees currUser = null;
            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            if (currUser != null && (currUser.Role == "Штаб-квартира" || currUser.Role == "Руководитель"))
            {
                List<String> userTerritories = currUser.Regions.Replace(", ", "!").Split('!').ToList();
                var employees = await db.Employees.Where(x => userTerritories.Contains(x.Regions) && !String.IsNullOrEmpty(x.Role)).OrderBy(x => x.Surname).ToListAsync();

                //plan
                //fact
                //uk

                List<int> _onplan = new List<int>();
                List<int> _plans = new List<int>();
                List<int> _planuk = new List<int>();
                List<int> _inprogress = new List<int>();
                List<int> _contractsign = new List<int>();
                List<int> _dostup = new List<int>();
                List<int> _facts = new List<int>();
                List<int> _factuk = new List<int>();

                //var houses = 

                //motivacia plan
                //motivaciya fact
                //motivaciya porog vipolneniya

                return View(employees);
            }
            else
            {
                return View();
            }
        }


        public async Task<ActionResult> FactAgreed(int? year, int? month)
        {
            int y = (year != null && year.HasValue) ? year.Value : DateTime.Now.Year;
            int m = (month != null && month.HasValue) ? month.Value : DateTime.Now.Month;

            ViewBag.Year = y;
            ViewBag.Month = m;

            Employees currUser = null;
            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            if (currUser != null && (currUser.Role == "Штаб-квартира" || currUser.Role == "Руководитель"))
            {
                List<String> userTerritories = currUser.Regions.Replace(", ", "!").Split('!').ToList();
                var houses = db.Houses.Where(x => x.FactDate != null && userTerritories.Contains(x.Region) && x.FactDate.Value.Month == m && x.FactDate.Value.Year == y).OrderByDescending(x => x.FactDate);
                var housesList = await houses.ToListAsync();
                var employeeList = new List<String>();

                if(housesList.Count > 0)
                {
                    var empls = await db.Employees.Where(e => (houses.Select(h => h.FactResponsible).Distinct()).Contains(e.Id)).Select(r => new { r.Id, r.Surname }).ToDictionaryAsync(r => r.Id, r => r.Surname);
                    foreach (var h in housesList)
                    {
                        if (h.FactResponsible != null && h.FactResponsible.HasValue)
                        {
                            employeeList.Add(empls[h.FactResponsible.Value]);
                        }
                        else
                        {
                            employeeList.Add("НЕ НАЙДЕН");
                        }
                    }
                    //var empls = await houses.Join(db.Employees, h => h.FactResponsible, e => e.Id, (h, e) => new { h, e }).Select(r => r.e.Surname).ToListAsync();
                    ViewBag.EmployeeList = employeeList;
                }
                return View(housesList);
            }
            else
            {
                return View();
            }
        }

        public async Task<ActionResult> EmployeesApproved(int? year, int? month)
        {
            int y = (year != null && year.HasValue) ? year.Value : DateTime.Now.Year;
            int m = (month != null && month.HasValue) ? month.Value : DateTime.Now.Month;

            ViewBag.Year = y;
            ViewBag.Month = m;

            Employees currUser = null;
            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            if (currUser != null && (currUser.Role == "Штаб-квартира" || currUser.Role == "Руководитель"))
            {
                List<String> userTerritories = currUser.Regions.Replace(", ", "!").Split('!').ToList();

                var houses = await db.Houses.Where(x => x.FactDate != null && x.FactResponsible != null && userTerritories.Contains(x.Region) && x.FactDate.Value.Month == m && x.FactDate.Value.Year == y).GroupBy(x => x.FactResponsible).Select(g => new { NegotiatorId = g.Key, ApprovedHouses = g.Count() }).OrderByDescending(g => g.ApprovedHouses).ToListAsync();

                List<int> respIds = new List<int>();

                foreach (var h in houses)
                {
                    respIds.Add(h.NegotiatorId.Value);
                }

                var employees = await db.Employees.Where(x => respIds.Contains(x.Id)).ToListAsync();

                List<Employees> result = new List<Employees>();
                List<int> apprHouses = new List<int>();

                foreach (var h in houses)
                {
                    var e = employees.Where(x => x.Id == h.NegotiatorId).FirstOrDefault();
                    result.Add(e);
                    apprHouses.Add(h.ApprovedHouses);
                }

                ViewBag.Results = apprHouses;
                return View(result);
            }
            else
            {
                return View();
            }            
        }

        public async Task<ActionResult> Motivation(int? year, int? month)
        {
            int y = (year != null && year.HasValue) ? year.Value : DateTime.Now.Year;
            int m = (month != null && month.HasValue) ? month.Value : DateTime.Now.Month;

            ViewBag.Year = y;
            ViewBag.Month = m;

            Employees currUser = null;
            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            if (currUser != null && (currUser.Role == "Штаб-квартира" || currUser.Role == "Руководитель"))
            {
                List<String> userTerritories = currUser.Regions.Replace(", ", "!").Split('!').ToList();

                var employeesplan = await db.Houses.Where(x => userTerritories.Contains(x.Region) && x.IsPersonal == true && x.IsPlanned == true && x.PlanApproval.HasValue && x.PlanApproval.Value.Year == y && x.PlanApproval.Value.Month == m).GroupBy(x => x.FactResponsible).Select(g => new { NegotiatorId = g.Key, PlannedHouses = g.Count() }).OrderByDescending(g => g.PlannedHouses).ToListAsync();
                var employeesfact = await db.Houses.Where(x => userTerritories.Contains(x.Region) && x.IsPersonal == true && x.FactDate.HasValue && x.FactDate.Value.Year == y && x.FactDate.Value.Month == m).GroupBy(x => x.FactResponsible).Select(g => new { NegotiatorId = g.Key, FactHouses = g.Count() }).ToListAsync();

                List<Employees> employees = new List<Employees>();
                List<int> plans = new List<int>();
                List<int> facts = new List<int>();
                List<int> results = new List<int>();

                foreach (var itm in employeesplan)
                {
                    var employee = await db.Employees.Where(x => x.Id == itm.NegotiatorId).FirstOrDefaultAsync();
                    employees.Add(employee);
                    plans.Add(itm.PlannedHouses);
                    int fct = employeesfact.Where(x => x.NegotiatorId == itm.NegotiatorId).Select(x => x.FactHouses).FirstOrDefault();
                    facts.Add(fct);
                    Double res = 1.0 * fct / itm.PlannedHouses * 100.0;
                    results.Add(Convert.ToInt32(res));
                }

                ViewBag.Empls = employees;
                ViewBag.Plans = plans;
                ViewBag.Facts = facts;
                ViewBag.Results = results;

                return View();
            }
            else
            {
                return View();
            }

            
        }

        public async Task<ActionResult> Funnel(int? year)
        {
            int y = (year != null && year.HasValue) ? year.Value : DateTime.Now.Year;

            ViewBag.Year = y;

            Employees currUser = null;
            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            if (currUser != null && (currUser.Role == "Штаб-квартира" || currUser.Role == "Руководитель"))
            {
                List<String> userTerritories = currUser.Regions.Replace(", ", "!").Split('!').ToList();
                var territories = await db.Houses.Where(x => userTerritories.Contains(x.Region) && x.RoYear == y).GroupBy(x => new { x.Region, x.Filial }).Select(g => new { Region = g.Key.Region, Filial = g.Key.Filial }).Distinct().OrderBy(x => x.Region).ThenBy(x => x.Filial).ToListAsync();

                List<String> Regions = new List<string>();
                List<String> Filials = new List<string>();
                List<int> _Total = new List<int>();
                List<int> _Possibility = new List<int>();
                List<int> _Planning = new List<int>();
                List<int> _PlanApproval = new List<int>();
                List<int> _AtWorkPersonal = new List<int>();
                List<int> _AtWorkNotPersonal = new List<int>();
                List<int> _AccessOnApproval = new List<int>();
                List<int> _AccessNotApproved = new List<int>();
                List<int> _DocumentSigning = new List<int>();
                List<int> _OnFactApproval = new List<int>();
                List<int?> _Master = new List<int?>();
                List<int> _Fact = new List<int>();
                List<int?> _Progress = new List<int?>();


                foreach (var t in territories)
                {
                    Regions.Add(t.Region);
                    Filials.Add(t.Filial);

                    var houses = db.Houses.Where(x => x.RoYear == y && x.Region == t.Region && x.Filial == t.Filial);
                    _Total.Add(await houses.CountAsync());
                    _Possibility.Add(await houses.Where(x => x.Status == "Проверка возможности договора с УК").CountAsync());
                    _Planning.Add(await houses.Where(x => x.FactTransferredToAgreement != null && x.IsPlanned != true).CountAsync());
                    _PlanApproval.Add(await houses.Where(x => x.FactTransferredToAgreement != null && x.IsPlanned != true && x.PlanApproval != null).CountAsync());
                    _AtWorkPersonal.Add(await houses.Where(x => x.IsPlanned == true && x.IsPersonal == true && x.FactDate == null).CountAsync());
                    _AtWorkNotPersonal.Add(await houses.Where(x => x.IsPlanned == true && x.IsPersonal != true && x.FactDate == null).CountAsync());
                    _AccessOnApproval.Add(await houses.Where(x => x.FactAgreed != null && x.FactApproved == null).CountAsync());
                    _AccessNotApproved.Add(await houses.Where(x => x.Status == "Нет доступа на объект").CountAsync());
                    _DocumentSigning.Add(await houses.Where(x => x.FactAgreed != null && (x.FactDocumentDate == null || String.IsNullOrEmpty(x.FactDocumentDescription) || String.IsNullOrEmpty(x.FactDocumentLink))).CountAsync());
                    _OnFactApproval.Add(await houses.Where(x => x.FactApproved != null && x.FactAgreed != null && x.FactDocumentDate != null && !String.IsNullOrEmpty(x.FactDocumentDescription) && !String.IsNullOrEmpty(x.FactDocumentLink)).CountAsync());

                    var master = db.MasterPlans.Where(m => m.Subject == "RO" && m.Year == y && m.Type == "Filial" && m.Filial == t.Filial);
                    int mCnt = await master.CountAsync();
                    int mPlan = 0;
                    if (mCnt > 0)
                    {
                        mPlan = Convert.ToInt32(await master.Select(m => m.Value).SumAsync());
                        _Master.Add(mPlan);
                    }
                    else
                    {
                        _Master.Add(null);
                    }

                    int fct = await houses.Where(x => x.FactDate != null).CountAsync();
                    _Fact.Add(fct);

                    if (mCnt > 0 && mPlan > 0)
                    {
                        _Progress.Add(Convert.ToInt32(100.0 * fct / mPlan));
                    }
                    else
                    {
                        _Progress.Add(null);
                    }
                }

                ViewBag.Regions = Regions;
                ViewBag.Filials = Filials;
                ViewBag.Total = _Total;
                ViewBag.Possibility = _Possibility;
                ViewBag.Planning = _Planning;
                ViewBag.PlanApproval = _PlanApproval;
                ViewBag.AtWorkPersonal = _AtWorkPersonal;
                ViewBag.AtWorkNotPersonal = _AtWorkNotPersonal;
                ViewBag.AccessOnApproval = _AccessOnApproval;
                ViewBag.AccessNotApproved = _AccessNotApproved;
                ViewBag.DocumentSigning = _DocumentSigning;
                ViewBag.OnFactApproval = _OnFactApproval;
                ViewBag.Master = _Master;
                ViewBag.Fact = _Fact;
                ViewBag.Progress = _Progress;


                return View();
            }
            else
            {
                return View();
            }
        }

        public async Task<ActionResult> Planning(int? year, int? take)
        {
            Employees currUser = null;
            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            int y = (year != null && year >= 2018) ? year.Value : DateTime.Now.Year;
            ViewBag.Year = y;

            int t = (take != null) ? take.Value : 100;
            ViewBag.Take = t;

            List<String> userTerritories = currUser.Filials.Replace(", ", "!").Split('!').ToList();

            if (currUser.Role == "Переговорщик")
            {
                var companies = db.ManagementCompanys.Where(x => userTerritories.Contains(x.Filial));

                if (y == 2018)
                {
                    companies = companies.Where(x => x.RO18Houses > 0).OrderByDescending(x => x.RO18Houses).Take(t);
                }

                if (y == 2019)
                {
                    companies = companies.Where(x => x.RO19Houses > 0).OrderByDescending(x => x.RO19Houses).Take(t);
                }

                return View(await companies.ToListAsync());
            }

            if (currUser.Role == "Руководитель")
            {
                var companies = db.ManagementCompanys.Where(x => x.Region == currUser.Regions);

                if (y == 2018)
                {
                    companies = companies.Where(x => x.RO18Houses > 0).OrderByDescending(x => x.RO18Houses).Take(t);
                }

                if (y == 2019)
                {
                    companies = companies.Where(x => x.RO19Houses > 0).OrderByDescending(x => x.RO19Houses).Take(t);
                }

                return View(await companies.ToListAsync());
            }

            if (currUser.Role == "Штаб-квартира")
            {
                if (y == 2018)
                {
                    var companies = db.ManagementCompanys.Where(x => x.RO18Houses > 0).OrderByDescending(x => x.RO18Houses).Take(t);
                    return View(await companies.ToListAsync());
                }

                if (y == 2019)
                {
                    var companies = db.ManagementCompanys.Where(x => x.RO19Houses > 0).OrderByDescending(x => x.RO19Houses).Take(t);
                    return View(await companies.ToListAsync());
                }

                return View();
            }

            /*var houses = await db.Houses.Where(x => x.RoYear == y && x.EmployeeId == currUser.Id && x.IsPlanned != true).GroupBy(g => new {g.UK, g.Filial}).Select(g => new { UK = g.Key, UKCount = g.Count(), Reg = g.FirstOrDefault().Region, Fil = g.FirstOrDefault().Filial }).OrderByDescending(n => n.UKCount).Take(100).ToDictionaryAsync(r => r.UK.UK + "|" + r.UK.Filial,  r => new Tuple<string, string, int>(r.Reg, r.Fil, r.UKCount));

            if (currUser.Role == "Штаб-квартира")
            {
                houses = await db.Houses.Where(x => x.RoYear == y && x.IsPlanned != true).GroupBy(g => new { g.UK, g.Filial }).Select(g => new { UK = g.Key, UKCount = g.Count(), Reg = g.FirstOrDefault().Region, Fil = g.FirstOrDefault().Filial }).OrderByDescending(n => n.UKCount).Take(100).ToDictionaryAsync(r => r.UK.UK + "|" + r.UK.Filial, r => new Tuple<string, string, int>(r.Reg, r.Fil, r.UKCount));
            }

            if (currUser.Role == "Руководитель")
            {
                houses = await db.Houses.Where(x => x.RoYear == y && x.Region == currUser.Regions && x.IsPlanned != true).GroupBy(g => new { g.UK, g.Filial }).Select(g => new { UK = g.Key, UKCount = g.Count(), Reg = g.FirstOrDefault().Region, Fil = g.FirstOrDefault().Filial }).OrderByDescending(n => n.UKCount).Take(100).ToDictionaryAsync(r => r.UK.UK + "|" + r.UK.Filial, r => new Tuple<string, string, int>(r.Reg, r.Fil, r.UKCount));
            }
            */

            
            return View();
        }

        public async Task<ActionResult> Map()
        {
            var houses = await db.Houses.FirstOrDefaultAsync();
            return View();
        }

        public async Task<ActionResult> HeatMap()
        {
            var houses = await db.Houses.Where(x => x.FactAgreed != null && x.FactAgreed.Value.Month == DateTime.Now.Month).CountAsync();
            return View();
        }

    }
}
