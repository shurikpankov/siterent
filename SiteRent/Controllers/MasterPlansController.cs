﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SiteRent.Models;

namespace SiteRent.Controllers
{
    [Authorize]
    public class MasterPlansController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MasterPlans
        public async Task<ActionResult> Index()
        {
            return View(await db.MasterPlans.ToListAsync());
        }

        // GET: MasterPlans/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterPlans masterPlans = await db.MasterPlans.FindAsync(id);
            if (masterPlans == null)
            {
                return HttpNotFound();
            }
            return View(masterPlans);
        }

        // GET: MasterPlans/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MasterPlans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Subject,Type,Year,Period,Region,Filial,EmployeeId,Value")] MasterPlans masterPlans)
        {
            if (ModelState.IsValid)
            {
                db.MasterPlans.Add(masterPlans);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(masterPlans);
        }

        // GET: MasterPlans/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterPlans masterPlans = await db.MasterPlans.FindAsync(id);
            if (masterPlans == null)
            {
                return HttpNotFound();
            }
            return View(masterPlans);
        }

        // POST: MasterPlans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Subject,Type,Year,Period,Region,Filial,EmployeeId,Value")] MasterPlans masterPlans)
        {
            if (ModelState.IsValid)
            {
                db.Entry(masterPlans).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(masterPlans);
        }

        // GET: MasterPlans/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MasterPlans masterPlans = await db.MasterPlans.FindAsync(id);
            if (masterPlans == null)
            {
                return HttpNotFound();
            }
            return View(masterPlans);
        }

        // POST: MasterPlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            MasterPlans masterPlans = await db.MasterPlans.FindAsync(id);
            db.MasterPlans.Remove(masterPlans);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
