﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SiteRent.Models;

namespace SiteRent.Controllers
{
    public class ManagementCompanysController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ManagementCompanys
        public async Task<ActionResult> Index()
        {
            return View(await db.ManagementCompanys.ToListAsync());
        }

        // GET: ManagementCompanys/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManagementCompanys managementCompanys = await db.ManagementCompanys.FindAsync(id);
            if (managementCompanys == null)
            {
                return HttpNotFound();
            }
            return View(managementCompanys);
        }

        // GET: ManagementCompanys/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ManagementCompanys/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Region,Filial,CityId,City,Name,Contacts,RO18Houses,RO19Houses")] ManagementCompanys managementCompanys)
        {
            if (ModelState.IsValid)
            {
                db.ManagementCompanys.Add(managementCompanys);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(managementCompanys);
        }

        // GET: ManagementCompanys/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManagementCompanys managementCompanys = await db.ManagementCompanys.FindAsync(id);
            if (managementCompanys == null)
            {
                return HttpNotFound();
            }
            return View(managementCompanys);
        }

        // POST: ManagementCompanys/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Region,Filial,CityId,City,Name,Contacts,RO18Houses,RO19Houses")] ManagementCompanys managementCompanys)
        {
            if (ModelState.IsValid)
            {
                db.Entry(managementCompanys).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(managementCompanys);
        }

        // GET: ManagementCompanys/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ManagementCompanys managementCompanys = await db.ManagementCompanys.FindAsync(id);
            if (managementCompanys == null)
            {
                return HttpNotFound();
            }
            return View(managementCompanys);
        }

        // POST: ManagementCompanys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ManagementCompanys managementCompanys = await db.ManagementCompanys.FindAsync(id);
            db.ManagementCompanys.Remove(managementCompanys);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
