﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SiteRent.Models;

namespace SiteRent.Controllers
{
    [Authorize]
    public class HousesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Houses
        public async Task<ActionResult> Index()
        {
            int page = 1;
            int year = DateTime.Now.Year;
            int status = 0;
            int plan = 0;
            int fact = 0;
            string search = Request["search"];
            int searchchanged = 0;
            string region = Request["region"];
            string filial = Request["filial"];
            string uk = Request["uk"];
            int employee = 0;

            Employees currUser = null;
            List<String> cuRegions = new List<string>();
            List<String> cuFilials = new List<string>();            

            var houses = db.Houses.Include(h => h.Employee);

            if (User.Identity.IsAuthenticated)
            {
                currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();                
            }
            if (currUser != null)
            {
                ViewBag.CurrentEmployee = currUser;
                cuRegions = currUser.Regions.Replace(", ", "!").Split('!').ToList();
                cuFilials = currUser.Filials.Replace(", ", "!").Split('!').ToList();                
            }

            var UserViewEmployees = await db.Employees.Where(e => cuRegions.Contains(e.Regions) && (e.Role == "Переговорщик" || e.Role == "Сопровождающий")).Select(e => new { EmployeeId = e.Id, EmployeeText = e.Surname + " " + e.Name.Substring(0, 1) + ". " + e.Patronymic.Substring(0, 1) + ". (" + e.Role.Substring(0, 1) + ")" }).OrderBy(e => e.EmployeeText).ToListAsync();
            ViewBag.UserViewEmployees = UserViewEmployees;

            if (currUser != null && (currUser.Role == "Руководитель" || currUser.Role == "Ассистент"))
            {
                houses = houses.Where(x => cuFilials.Contains(x.Filial));
            }

            if (currUser != null && (currUser.Role == "Переговорщик" || currUser.Role == "Сопровождающий"))
            {
                houses = houses.Where(x => x.Employee.Email == User.Identity.Name);
            }

            //ViewBag.TotalHouses = houses.Count();

            if (!String.IsNullOrEmpty(Request["page"]) && int.TryParse(Request["page"], out page))
            {

            }
            else
            {
                page = 1;
            }

            if (!String.IsNullOrEmpty(Request["year"]) && int.TryParse(Request["year"], out year) && year >= 2015)
            {

            }
            else
            {
                year = DateTime.Now.Year;
            }

            if (!String.IsNullOrEmpty(Request["status"]) && int.TryParse(Request["status"], out status))
            {

            }
            else
            {
                status = 0;
            }

            if (!String.IsNullOrEmpty(Request["plan"]) && int.TryParse(Request["plan"], out plan))
            {

            }
            else
            {
                plan = 0;
            }

            if (!String.IsNullOrEmpty(Request["fact"]) && int.TryParse(Request["fact"], out fact))
            {

            }
            else
            {
                fact = 0;
            }

            if (!String.IsNullOrEmpty(Request["searchchanged"]) && Request["searchchanged"] == "1")
            {
                page = 1;
            }

            if (!String.IsNullOrEmpty(Request["employee"]) && int.TryParse(Request["employee"], out employee) && employee >= -1)
            {

            }
            else
            {
                employee = 0;
            }

            if (currUser != null && currUser.Role == "Штаб-квартира" && !String.IsNullOrEmpty(region) && cuRegions.IndexOf(region) > -1 && (String.IsNullOrEmpty(filial) || cuFilials.IndexOf(filial) == -1))
            {
                houses = houses.Where(x => x.Region == region);
            }

            if (currUser != null && (currUser.Role == "Руководитель" || currUser.Role == "Штаб-квартира") && !String.IsNullOrEmpty(filial) && cuFilials.IndexOf(filial) > -1)
            {
                houses = houses.Where(x => x.Filial == filial);
            }

            if (currUser != null && (currUser.Role == "Руководитель" || currUser.Role == "Штаб-квартира") && employee != 0)
            {
                if (UserViewEmployees.Where(e => e.EmployeeId == employee).Count() > 0)
                {
                    houses = houses.Where(x => x.EmployeeId == employee);
                }
                else
                {
                    if (employee == -1)
                    {
                        List<int> uve = UserViewEmployees.Select(e => e.EmployeeId).ToList();
                        houses = houses.Where(x => !uve.Contains(x.EmployeeId.Value));
                    }
                }
            }

            if (year >= 2015)
            {
                houses = houses.Where(x => x.RoYear == year);
            }

            switch (status)
            {
                case 0: break;
                case 1: houses = houses.Where(x => x.Status == "Проверка возможности договора с УК"); break;
                case 2: houses = houses.Where(x => x.Status == "Согласование с УК"); break;
                case 3: houses = houses.Where(x => x.Status == "Не согласован с УК"); break;
                case 4: houses = houses.Where(x => x.Status == "Проверка доступа на дом"
                || x.Status == "Нет доступа на объект"
                || x.Status == "Проведение ПИР"
                || x.Status == "Ждем согласование ПИР с проектировщиками ШК"
                || x.Status == "Доработка ПИР"
                || x.Status == "Передача ПИР в УК"
                || x.Status == "Ждем согласования ПИР от УК"
                || x.Status == "ПИР не согласован с УК"
                || x.Status == "Передача дома в СМР"
                || x.Status == "Проведение СМР"
                || x.Status == "Ждем возобновления СМР"
                || x.Status == "Подварка"
                || x.Status == "Приемка работ"
                || x.Status == "Устранение замечаний"
                || x.Status == "Подготовка документов на активацию"
                || x.Status == "На активации"
                || x.Status == "Активирован"); break;
                default: break;
            }

            switch (plan)
            {
                case 0: break;
                case 1: houses = houses.Where(x => x.IsPlanned == true); break;
                case 2: houses = houses.Where(x => x.PlanApproval != null && x.IsPlanned == false); break;
                case 3: houses = houses.Where(x => x.PlanApproval == null && x.FactTransferredToAgreement != null); break;
                default: break;
            }

            switch (fact)
            {
                case 0: break;
                case 1: houses = houses.Where(x => x.IsApproved == true); break;
                case 2: houses = houses.Where(x => x.IsApproved == false && x.FactAgreed != null && x.FactApproved != null && x.FactDocumentDate != null && !String.IsNullOrEmpty(x.FactDocumentLink) && !String.IsNullOrEmpty(x.FactDocumentDescription)); break;
                case 3: houses = houses.Where(x => x.IsPlanned == true && x.IsApproved == false && x.FactAgreed == null); break;
                case 4: houses = houses.Where(x => x.IsPlanned == true && x.IsApproved == false && x.FactApproved == null); break;
                case 5: houses = houses.Where(x => x.IsPlanned == true && x.IsApproved == false && (x.FactDocumentDate == null || String.IsNullOrEmpty(x.FactDocumentLink) || String.IsNullOrEmpty(x.FactDocumentDescription))); break;
                default: break;
            }

            if (!String.IsNullOrEmpty(search))
            {
                houses = houses.Where(x => x.StreetName.Contains(search) || x.City.Contains(search) || x.Area.Contains(search) || x.Status.Contains(search) || x.Number1.Contains(search) || x.GFK.Contains(search) || x.ERP.Contains(search) || x.UK.Contains(search));
            }

            if(houses != null)
            {
                ViewBag.TotalCount = houses.Count();
            }
            else
            {
                ViewBag.TotalCount = 0;
            }

            houses = houses.OrderByDescending(x => x.Id).Skip(15 * (page - 1)).Take(15);

            ViewBag.page = page;

            return View(await houses.ToListAsync());
        }

        // GET: Houses
        public async Task<ActionResult> State()
        {
            //var houses = db.Houses.Include(h => h.Employee);
            //return View(await houses.ToListAsync());

            var houses = db.Houses.Where(x => x.RoYear == 2018);

            List<String> RegionNamesList = new List<string>();
            List<int> RegionPlansList = new List<int>();

            RegionNamesList.Add("Большая Москва");
            RegionNamesList.Add("Юг");
            RegionNamesList.Add("Центр");
            RegionNamesList.Add("Запад");
            RegionNamesList.Add("Восток");

            foreach (string reg in RegionNamesList)
            {
                var mPLl = db.MasterPlans.Where(x => x.Subject == "RO" && x.Type == "Filial" && x.Region == reg && x.Period <= DateTime.Now.Month && x.Year == DateTime.Now.Year).Select(x => x.Value);
                if (await mPLl.CountAsync() > 0)
                {
                    RegionPlansList.Add(Convert.ToInt32(await mPLl.SumAsync()));
                }
                else
                {
                    RegionPlansList.Add(0);
                }
            }

            /*List<int> TotalRegionHouses = new List<int>();
            foreach (string reg in RegionNamesList)
            {
                TotalRegionHouses.Add(db.Houses.Count(x => x.Region == reg));
            }*/

            List<int> RO18RegionHouses = new List<int>();
            foreach (string reg in RegionNamesList)
            {
                RO18RegionHouses.Add(await houses.Where(x => x.Region == reg).CountAsync());
            }

            List<int> RO18RegionOnApproval = new List<int>();
            foreach (string reg in RegionNamesList)
            {
                RO18RegionOnApproval.Add(await houses.Where(x => x.Region == reg && x.Status == "Согласование с УК").CountAsync());
            }

            List<int> RO18RegionApproved = new List<int>();
            foreach (string reg in RegionNamesList)
            {
                RO18RegionApproved.Add(await houses.Where(x => x.Region == reg &&
                (x.Status == "Проверка доступа на дом"
                || x.Status == "Нет доступа на объект"
                || x.Status == "Проведение ПИР"
                || x.Status == "Ждем согласование ПИР с проектировщиками ШК"
                || x.Status == "Доработка ПИР"
                || x.Status == "Передача ПИР в УК"
                || x.Status == "Ждем согласования ПИР от УК"
                || x.Status == "ПИР не согласован с УК"
                || x.Status == "Передача дома в СМР"
                || x.Status == "Проведение СМР"
                || x.Status == "Ждем возобновления СМР"
                || x.Status == "Подварка"
                || x.Status == "Приемка работ"
                || x.Status == "Устранение замечаний"
                || x.Status == "Подготовка документов на активацию"
                || x.Status == "На активации"
                || x.Status == "Активирован")).CountAsync());
            }

            List<int> RO18RegionApprovedThisMonth = new List<int>();
            foreach (string reg in RegionNamesList)
            {
                RO18RegionApprovedThisMonth.Add(await houses.Where(x => x.Region == reg && x.FactAgreed.Value.Month == DateTime.Now.Month &&
                (x.Status == "Проверка доступа на дом"
                || x.Status == "Нет доступа на объект"
                || x.Status == "Проведение ПИР"
                || x.Status == "Ждем согласование ПИР с проектировщиками ШК"
                || x.Status == "Доработка ПИР"
                || x.Status == "Передача ПИР в УК"
                || x.Status == "Ждем согласования ПИР от УК"
                || x.Status == "ПИР не согласован с УК"
                || x.Status == "Передача дома в СМР"
                || x.Status == "Проведение СМР"
                || x.Status == "Ждем возобновления СМР"
                || x.Status == "Подварка"
                || x.Status == "Приемка работ"
                || x.Status == "Устранение замечаний"
                || x.Status == "Подготовка документов на активацию"
                || x.Status == "На активации"
                || x.Status == "Активирован")).CountAsync());
            }

            List<int> RO18RegionApprovedThisWeek = new List<int>();
            DateTime LastWeekDate = DateTime.Now.AddDays(-7);
            foreach (string reg in RegionNamesList)
            {
                RO18RegionApprovedThisWeek.Add(await houses.Where(x => x.Region == reg && x.FactAgreed >= LastWeekDate &&
                (x.Status == "Проверка доступа на дом"
                || x.Status == "Нет доступа на объект"
                || x.Status == "Проведение ПИР"
                || x.Status == "Ждем согласование ПИР с проектировщиками ШК"
                || x.Status == "Доработка ПИР"
                || x.Status == "Передача ПИР в УК"
                || x.Status == "Ждем согласования ПИР от УК"
                || x.Status == "ПИР не согласован с УК"
                || x.Status == "Передача дома в СМР"
                || x.Status == "Проведение СМР"
                || x.Status == "Ждем возобновления СМР"
                || x.Status == "Подварка"
                || x.Status == "Приемка работ"
                || x.Status == "Устранение замечаний"
                || x.Status == "Подготовка документов на активацию"
                || x.Status == "На активации"
                || x.Status == "Активирован")).CountAsync());
            }


            List<string> RO18RegionFactPercent = new List<string>();

            for (int i = 0; i < RegionNamesList.Count; i++)
            {
                decimal currFactPercernt = decimal.One;
                currFactPercernt = (RegionPlansList[i] > 0) ? decimal.One * RO18RegionApproved[i] * 100 / RegionPlansList[i] : -1;
                RO18RegionFactPercent.Add(currFactPercernt.ToString().Split(',')[0].Split('.')[0] + "%");
            }

            string currentMonth = String.Empty;
            switch (DateTime.Now.Month)
            {
                case 1: currentMonth = "ЯНВ"; break;
                case 2: currentMonth = "ФЕВ"; break;
                case 3: currentMonth = "МАР"; break;
                case 4: currentMonth = "АПР"; break;
                case 5: currentMonth = "МАЙ"; break;
                case 6: currentMonth = "ИЮН"; break;
                case 7: currentMonth = "ИЮЛ"; break;
                case 8: currentMonth = "АВГ"; break;
                case 9: currentMonth = "СЕН"; break;
                case 10: currentMonth = "ОКТ"; break;
                case 11: currentMonth = "НОЯ"; break;
                case 12: currentMonth = "ДЕК"; break;
                default: break;

            }

            //ViewBag.TotalHouses = db.Houses.Count();
            ViewBag.RegionNamesList = RegionNamesList;
            //ViewBag.TotalRegionHouses = TotalRegionHouses;
            ViewBag.RO18RegionHouses = RO18RegionHouses;
            ViewBag.RO18RegionOnApproval = RO18RegionOnApproval;
            ViewBag.RegionPlansList = RegionPlansList;
            ViewBag.RO18RegionApproved = RO18RegionApproved;
            ViewBag.RO18RegionFactPercent = RO18RegionFactPercent;
            ViewBag.RO18RegionApprovedThisMonth = RO18RegionApprovedThisMonth;
            ViewBag.RO18RegionApprovedThisWeek = RO18RegionApprovedThisWeek;
            ViewBag.CurrenStrMonth = currentMonth;


            return View(await houses.FirstOrDefaultAsync());
        }

        // GET: Houses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Houses houses = await db.Houses.FindAsync(id);
            if (houses == null)
            {
                return HttpNotFound();
            }
            return View(houses);
        }

        // GET: Houses/Create
        public ActionResult Create()
        {
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "Email");
            return View();
        }

        // POST: Houses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,NriId,Region,Filial,CityId,City,AreaId,Area,StreetName,StreetType,Number1,Type1,Number2,Type2,Number3,Type3,GFK,ERP,RoYear,Status,PlanApproval,FactTransferredToAgreement,FactAgreed,FactApproved,Flats,Floors,Entrances,PlanAgreementPrice,PoNumber,PoString,FactAgreementPrice,CapexReliased,PlanRentPrice,FactRentPrice,UK,UkContacts,NeedOss,NeedPir,HasOss,HasPir,Latitude,Longitude,Comment,EmployeeId,HasContractAdvertising,HasContractAdvancedAccess")] Houses houses)
        {
            if (ModelState.IsValid)
            {
                db.Houses.Add(houses);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "Email", houses.EmployeeId);
            return View(houses);
        }

        // GET: Houses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Houses houses = await db.Houses.FindAsync(id);
            if (houses == null)
            {
                return HttpNotFound();
            }
            ViewBag.EmployeeId = new SelectList(db.Employees, "Id", "Email", houses.EmployeeId);
            if (User.Identity.IsAuthenticated)
            {
                Employees currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }
            if(houses.FactResponsible != null)
            {
                Employees respUser = db.Employees.Where(x => x.Id == houses.FactResponsible).FirstOrDefault();
                ViewBag.ResponsibleEmployee = respUser;
            }
            if (!String.IsNullOrEmpty(houses.EcmDocumentId))
            {
                EcmDocuments doc = db.EcmDocuments.Where(x => x.Id == houses.EcmDocumentId).FirstOrDefault();
                ViewBag.EcmDocument = doc;
            }
            return View(houses);
        }

        // POST: Houses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,EmployeeId,PlanApproval,IsPlanned,IsPersonal,FactDate,FactResponsible,IsApproved,FactAgreementPrice,FactRentPrice,HasContractAdvertising,HasContractAdvancedAccess")] Houses houses, string submit, bool chbIsPersonal, bool chbHasContractAdvertising, bool chbHasContractAdvancedAccess)
        {
            if (User.Identity.IsAuthenticated)
            {
                Employees currUser = db.Employees.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                ViewBag.CurrentEmployee = currUser;
            }

            switch (submit)
            {
                case "Сохранить":
                    if (ModelState.IsValid)
                    {
                        Houses h = db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault();

                        if (!h.EmployeeId.HasValue || h.Employee == null)
                        {
                            ViewBag.Message = "Ответственный за дом не установлен";
                            ModelState.Clear();
                            return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                        }

                        if (h.Employee.Role != "Переговорщик" && chbIsPersonal == true)
                        {
                            ViewBag.Message = "В мотивационный программе может участвовать только Переговорщик";
                            ModelState.Clear();
                            return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                        }

                        h.IsPersonal = chbIsPersonal;
                        h.HasContractAdvertising = chbHasContractAdvertising;
                        h.HasContractAdvancedAccess = chbHasContractAdvancedAccess;
                        h.FactAgreementPrice = houses.FactAgreementPrice;
                        h.FactRentPrice = houses.FactRentPrice;


                        db.Entry(h).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");                        
                    }
                    else
                    {
                        ViewBag.Message = "Введены неверные данные";
                        ModelState.Clear();
                        return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                    }
                case "Утвердить план":
                    if (ModelState.IsValid)
                    {
                        Houses h = db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault();
                        if (h.PlanApproval != null && h.PlanApproval.HasValue && h.PlanApproval.Value.Year == DateTime.Now.Year && h.PlanApproval.Value.Month > DateTime.Now.Month)
                        {
                            if (h.Employee.Role != "Переговорщик" && chbIsPersonal == true)
                            {
                                ViewBag.Message = "В мотивационный программе может участвовать только Переговорщик";
                                return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                            }

                            h.IsPlanned = true;
                            h.PlanApproval = houses.PlanApproval;
                            h.IsPersonal = chbIsPersonal;

                            h.FactResponsible = h.EmployeeId;
                            Employees respUser = db.Employees.Where(x => x.Id == h.EmployeeId).FirstOrDefault();
                            ViewBag.ResponsibleEmployee = respUser;

                            db.Entry(h).State = EntityState.Modified;
                            await db.SaveChangesAsync();
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ViewBag.Message = "Некорректный период планирования. Объект может быть утвержден только в план будующих периодов.";
                            return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                        }                        
                    }
                    else
                    {
                        ViewBag.Message = "Введены неверные данные";
                        return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                    }
                case "Подтвердить факт":
                    if (ModelState.IsValid)
                    {
                        Houses h = db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault();
                        if (h.FactAgreed != null && (h.FactApproved != null || (h.FactAgreed != null && h.FactAgreed.HasValue && h.FactAgreed.Value.AddDays(14) < DateTime.Now && h.Status == "Проверка доступа на дом")) && h.FactDocumentDate != null && !String.IsNullOrEmpty(h.FactDocumentDescription) && !String.IsNullOrEmpty(h.FactDocumentLink))
                        {
                            if (houses.FactAgreementPrice != null && houses.FactAgreementPrice.HasValue && houses.FactAgreementPrice.Value >= 0 && houses.FactRentPrice != null && houses.FactRentPrice.HasValue && houses.FactRentPrice.Value >= 0)
                            {
                                if (h.EmployeeId.HasValue && h.Employee != null)
                                {
                                    h.FactDate = DateTime.Now;
                                    h.IsPersonal = chbIsPersonal;
                                    h.IsApproved = true;
                                    h.FactAgreementPrice = houses.FactAgreementPrice;
                                    h.FactRentPrice = houses.FactRentPrice;
                                    h.HasContractAdvertising = chbHasContractAdvertising;
                                    h.HasContractAdvancedAccess = chbHasContractAdvancedAccess;

                                    db.Entry(h).State = EntityState.Modified;
                                    await db.SaveChangesAsync();
                                    return RedirectToAction("Index");
                                }
                                else
                                {
                                    ViewBag.Message = "Ответственный за дом не установлен";
                                    ModelState.Clear();
                                    return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                                }                                
                            }
                            else
                            {
                                ViewBag.Message = "Введены некорректные фактические затраты.";
                                ModelState.Clear();
                                return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                            }
                        }
                        else
                        {
                            ViewBag.Message = "Подтверждение невозможно. Дом не согласован в NRI, отсутствет договор в NRI либо досуп на объект в течение 14 дней не подтвержден стройкой.";
                            ModelState.Clear();
                            return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Введены неверные данные";
                        ModelState.Clear();
                        return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
                    }
                default:
                    ViewBag.Message = "Что то пошло не так!";
                    ModelState.Clear();
                    return View(db.Houses.Where(x => x.Id == houses.Id).FirstOrDefault());
            }
        }

        // GET: Houses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Houses houses = await db.Houses.FindAsync(id);
            if (houses == null)
            {
                return HttpNotFound();
            }
            return View(houses);
        }

        // POST: Houses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Houses houses = await db.Houses.FindAsync(id);
            db.Houses.Remove(houses);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }        
    }
}

/*
 OLD INDEX ON STATUSES

    @if(item.Status == "Проработка невозможна"
                            || item.Status == "Новый"
                            || item.Status == "Оценка возможности строительства"
                            || item.Status == "Проверка возможности договора с УК"
                            || item.Status == "Проверка возможности подведения транспорта"
                            || item.Status == "Транспорт невозможен"
                            || item.Status == "Договор невозможен"
                            || item.Status == "Оценка окупаемости"
                            || item.Status == "Не прошел окупаемость"
                            || item.Status == "Оценка МО нуждается в доработке"
                            || item.Status == "Проверка оценки окупаемости")
                        {
                            <img src="~/Content/Images/sandclock.svg" height="16" title="В оценке"/>
                        }

                        @if((item.PlanApproval == null || !item.PlanApproval.HasValue) 
                            && (item.Status == "Согласование с УК"
                            || item.Status == "Не согласован с УК"
                            || item.Status == "Проверка доступа на дом"
                            || item.Status == "Нет доступа на объект"
                            || item.Status == "Проведение ПИР"
                            || item.Status == "Ждем согласование ПИР с проектировщиками ШК"
                            || item.Status == "Доработка ПИР"
                            || item.Status == "Передача ПИР в УК"
                            || item.Status == "Ждем согласования ПИР от УК"
                            || item.Status == "ПИР не согласован с УК"
                            || item.Status == "Передача дома в СМР"
                            || item.Status == "Проведение СМР"
                            || item.Status == "Ждем возобновления СМР"
                            || item.Status == "Подварка"
                            || item.Status == "Приемка работ"
                            || item.Status == "Устранение замечаний"
                            || item.Status == "Подготовка документов на активацию"
                            || item.Status == "На активации"
                            || item.Status == "Активирован"))
                        {
                            <img src="~/Content/Images/nocalendar.svg" height="16" title="План не установлен" />
                        }

 */
