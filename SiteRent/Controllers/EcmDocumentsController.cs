﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SiteRent.Models;

namespace SiteRent.Controllers
{
    public class EcmDocumentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EcmDocuments
        public async Task<ActionResult> Index()
        {
            return View(await db.EcmDocuments.ToListAsync());
        }

        // GET: EcmDocuments/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EcmDocuments ecmDocuments = await db.EcmDocuments.FindAsync(id);
            if (ecmDocuments == null)
            {
                return HttpNotFound();
            }
            return View(ecmDocuments);
        }

        // GET: EcmDocuments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EcmDocuments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Status,Type,Landlord,INN,NumberBase,NumberAddon,SevdId,SevdLink,StartDate,Price,Currency,NDS,ZpNumber,SystemNumber,EcmComment,SignDate,ZpNfsLink")] EcmDocuments ecmDocuments)
        {
            if (ModelState.IsValid)
            {
                db.EcmDocuments.Add(ecmDocuments);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(ecmDocuments);
        }

        // GET: EcmDocuments/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EcmDocuments ecmDocuments = await db.EcmDocuments.FindAsync(id);
            if (ecmDocuments == null)
            {
                return HttpNotFound();
            }
            return View(ecmDocuments);
        }

        // POST: EcmDocuments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Status,Type,Landlord,INN,NumberBase,NumberAddon,SevdId,SevdLink,StartDate,Price,Currency,NDS,ZpNumber,SystemNumber,EcmComment,SignDate,ZpNfsLink")] EcmDocuments ecmDocuments)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ecmDocuments).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(ecmDocuments);
        }

        // GET: EcmDocuments/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EcmDocuments ecmDocuments = await db.EcmDocuments.FindAsync(id);
            if (ecmDocuments == null)
            {
                return HttpNotFound();
            }
            return View(ecmDocuments);
        }

        // POST: EcmDocuments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            EcmDocuments ecmDocuments = await db.EcmDocuments.FindAsync(id);
            db.EcmDocuments.Remove(ecmDocuments);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
