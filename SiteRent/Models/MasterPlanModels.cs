﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteRent.Models
{
    public class MasterPlans
    {
        [Key]
        public int Id { get; set; }
        public string Subject { get; set; } //Example: "RO", "aRFS"
        public string Type { get; set; } //Example: "week", "month", "year", "week_cum", "month_cum"
        public int Year { get; set; }
        public int Period { get; set; }
        public string Region { get; set; }
        public string Filial { get; set; }
        public int? EmployeeId { get; set; }
        public double Value { get; set; }
    }
}