﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteRent.Models
{
    public class DatabaseStates
    {
        [Key]
        public int Id { get; set; }
        public string State { get; set; }
        public int Progress { get; set; }
        public bool Success { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SourceDate { get; set; }
    }
}