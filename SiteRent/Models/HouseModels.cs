﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteRent.Models
{
    public class Houses
    {
        [Key]
        public int Id { get; set; } 
        public int NriId { get; set; }

        public string Region { get; set; }
        public string Filial { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }
        public int AreaId { get; set; }
        public string Area { get; set; }
        public string StreetName { get; set; }
        public string StreetType { get; set; }
        public string Number1 { get; set; }
        public string Type1 { get; set; }
        public string Number2 { get; set; }
        public string Type2 { get; set; }
        public string Number3 { get; set; }
        public string Type3 { get; set; }

        public string GFK { get; set; }
        public string ERP { get; set; }

        public int RoYear { get; set; }
        public string Status { get; set; }


        //Planning start
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FactTransferredToAgreement { get; set; }
        public bool IsPersonal { get; set; }
        [DataType(DataType.DateTime, ErrorMessage = "Неверный формат даты")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? PlanApproval { get; set; }
        public bool IsPlanned { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FrcApproval { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FactAgreed { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FactApproved { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FactDocumentDate { get; set; }
        public string FactDocumentLink { get; set; }
        public string FactDocumentDescription { get; set; }
        public string EcmDocumentId { get; set; } //ID документа в ECM

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FactDate { get; set; }
        public int? FactResponsible { get; set; }
        public bool IsApproved { get; set; }

        public bool HasContractAdvertising { get; set; }
        public bool HasContractAdvancedAccess { get; set; }

        public int PlanningScore { get; set; }
        //Planning end

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CreatedDate { get; set; }
        public int? AgreementCircle { get; set; } // Повторно в согласовании

        public int? Flats { get; set; }
        public int? Floors { get; set; }
        public int? Entrances { get; set; }

        public decimal? PlanAgreementPrice { get; set; }               
        public int? PoNumber { get; set; }
        public int? PoString { get; set; }
        public decimal? FactAgreementPrice { get; set; }
        public decimal? CapexReliased { get; set; }

        public decimal? PlanRentPrice { get; set; }
        public decimal? FactRentPrice { get; set; }

        public string UK { get; set; }
        public string UkContacts { get; set; }

        public bool? NeedOss { get; set; }
        public bool? NeedPir { get; set; }

        public bool? HasOss { get; set; }
        public bool? HasPir { get; set; }

        [My.Data.Annotations.Precision(18, 15)]
        public decimal? Latitude { get; set; }
        [My.Data.Annotations.Precision(18, 15)]
        public decimal? Longitude { get; set; }

        public string Comment { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? NriRentPlan { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? NriRentFact { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Updated { get; set; }

        public int? EmployeeId { get; set; } //KeyChange!!!
        public virtual Employees Employee { get; set; }
    }
}

namespace My.Data.Annotations

{

    /// <summary>

    /// The Precision class allows us to decorate our Entity Models with a Precision attribute 

    /// to specify decimal precision values for the database column

    /// </summary>

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]

    public class Precision : Attribute

    {

        /// <summary>

        /// The total number of digits to store, including decimals

        /// </summary>

        public byte precision { get; set; }

        /// <summary>

        /// The number of digits from the precision to be used for decimals

        /// </summary>

        public byte scale { get; set; }



        /// <summary>

        /// Define the precision and scale of a decimal data type

        /// </summary>

        /// <param name="precision">The total number of digits to store, including decimals</param>

        /// <param name="scale">The number of digits from the precision to be used for decimals</param>

        public Precision(byte precision, byte scale)

        {

            this.precision = precision;

            this.scale = scale;

        }



        /// <summary>

        /// Apply the precision to our data model for any property using this annotation

        /// </summary>

        /// <param name="modelBuilder"></param>

        public static void ConfigureModelBuilder(System.Data.Entity.DbModelBuilder modelBuilder)

        {

            modelBuilder.Properties().Where(x => x.GetCustomAttributes(false).OfType<Precision>().Any())

                .Configure(c => c.HasPrecision(c.ClrPropertyInfo.GetCustomAttributes(false).OfType<Precision>().First()

                    .precision, c.ClrPropertyInfo.GetCustomAttributes(false).OfType<Precision>().First().scale));

        }

    }

}