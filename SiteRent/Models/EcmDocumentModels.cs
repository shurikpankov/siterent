﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteRent.Models
{
    public class EcmDocuments
    {
        [Key]
        public string Id { get; set; } //"r_object_id"
        public string Status { get; set; } //"Статус"
        public string Type { get; set; } //"Тип документа"
        public string Landlord { get; set; } //"Контрагент"
        public string INN { get; set; } //"Инн контрагента"
        public string NumberBase { get; set; } //"Номер договора"
        public string NumberAddon { get; set; } //"Номер договора"
        public string SevdId { get; set; } //"Номер СЭВД"
        public string SevdLink { get; set; } //"Ссылка СЭВД"
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; } //"Дата начала"
        public decimal? Price { get; set; } //"Сумма договора"
        public string Currency { get; set; } //"Валюта договора"
        public string NDS { get; set; } //"НДС"
        public int? ZpNumber { get; set; } //"Номер ЗП"
        public string SystemNumber { get; set; } //"Системный номер"
        public string EcmComment { get; set; } //"Резюме"
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SignDate { get; set; } //"Дата фактического подписания"
        public string ZpNfsLink { get; set; } //"Ссылка на ЗП в НФС"

    }
}