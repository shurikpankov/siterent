﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteRent.Models
{
    public class ManagementCompanys
    {
        [Key]
        public int Id { get; set; }
        public string Region { get; set; }
        public string Filial { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }

        public string Name { get; set; }
        public string Contacts { get; set; }

        public int RO18Houses { get; set; }
        public int RO19Houses { get; set; }
    }
}