namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNriIdsToEmployeesAndHouses : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "NriId", c => c.Int(nullable: false));
            AddColumn("dbo.Houses", "NriId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Houses", "NriId");
            DropColumn("dbo.Employees", "NriId");
        }
    }
}
