namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedFactfixCreationdateAndcircle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Houses", "FactDate", c => c.DateTime());
            AddColumn("dbo.Houses", "FactResponsible", c => c.Int());
            AddColumn("dbo.Houses", "CreatedDate", c => c.DateTime());
            AddColumn("dbo.Houses", "AgreementCircle", c => c.Int());
            DropColumn("dbo.Houses", "FACT");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Houses", "FACT", c => c.DateTime());
            DropColumn("dbo.Houses", "AgreementCircle");
            DropColumn("dbo.Houses", "CreatedDate");
            DropColumn("dbo.Houses", "FactResponsible");
            DropColumn("dbo.Houses", "FactDate");
        }
    }
}
