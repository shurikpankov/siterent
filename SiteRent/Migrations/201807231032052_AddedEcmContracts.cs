namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEcmContracts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EcmDocuments",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Status = c.String(),
                        Type = c.String(),
                        Landlord = c.String(),
                        INN = c.String(),
                        NumberBase = c.String(),
                        NumberAddon = c.String(),
                        SevdId = c.String(),
                        SevdLink = c.String(),
                        StartDate = c.DateTime(),
                        Price = c.Decimal(precision: 18, scale: 2),
                        Currency = c.String(),
                        NDS = c.String(),
                        ZpNumber = c.Int(),
                        SystemNumber = c.String(),
                        EcmComment = c.String(),
                        SignDate = c.DateTime(),
                        ZpNfsLink = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Houses", "EcmDocumentId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Houses", "EcmDocumentId");
            DropTable("dbo.EcmDocuments");
        }
    }
}
