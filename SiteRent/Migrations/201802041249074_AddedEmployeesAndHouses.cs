namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEmployeesAndHouses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Surname = c.String(),
                        Name = c.String(),
                        Patronymic = c.String(),
                        Role = c.String(),
                        Regions = c.String(),
                        Filials = c.String(),
                        LastLoginDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Houses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Region = c.String(),
                        Filial = c.String(),
                        CityId = c.Int(nullable: false),
                        City = c.String(),
                        AreaId = c.Int(nullable: false),
                        Area = c.String(),
                        StreetName = c.String(),
                        StreetType = c.String(),
                        Number1 = c.String(),
                        Type1 = c.String(),
                        Number2 = c.String(),
                        Type2 = c.String(),
                        Number3 = c.String(),
                        Type3 = c.String(),
                        GFK = c.String(),
                        ERP = c.String(),
                        RoYear = c.Int(nullable: false),
                        Status = c.String(),
                        PlanApproval = c.DateTime(),
                        FactTransferredToAgreement = c.DateTime(),
                        FactAgreed = c.DateTime(),
                        FactApproved = c.DateTime(),
                        Flats = c.Int(),
                        Floors = c.Int(),
                        Entrances = c.Int(),
                        PlanAgreementPrice = c.Decimal(precision: 18, scale: 2),
                        PoNumber = c.Int(),
                        PoString = c.Int(),
                        FactAgreementPrice = c.Decimal(precision: 18, scale: 2),
                        CapexReliased = c.Decimal(precision: 18, scale: 2),
                        PlanRentPrice = c.Decimal(precision: 18, scale: 2),
                        FactRentPrice = c.Decimal(precision: 18, scale: 2),
                        UK = c.String(),
                        UkContacts = c.String(),
                        NeedOss = c.Boolean(),
                        NeedPir = c.Boolean(),
                        HasOss = c.Boolean(),
                        HasPir = c.Boolean(),
                        Latitude = c.Decimal(precision: 18, scale: 15),
                        Longitude = c.Decimal(precision: 18, scale: 15),
                        Comment = c.String(),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Houses", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Houses", new[] { "EmployeeId" });
            DropTable("dbo.Houses");
            DropTable("dbo.Employees");
        }
    }
}
