namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedLastLoginDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employees", "LastLoginDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employees", "LastLoginDate", c => c.DateTime(nullable: false));
        }
    }
}
