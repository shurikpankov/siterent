namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedEmployeeStartEndDates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "WorkingStartDate", c => c.DateTime());
            AddColumn("dbo.Employees", "WorkingEndDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "WorkingEndDate");
            DropColumn("dbo.Employees", "WorkingStartDate");
        }
    }
}
