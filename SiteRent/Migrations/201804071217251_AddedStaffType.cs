namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStaffType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "StaffType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "StaffType");
        }
    }
}
