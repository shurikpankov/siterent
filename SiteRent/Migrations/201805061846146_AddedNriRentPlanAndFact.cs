namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNriRentPlanAndFact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Houses", "NriRentPlan", c => c.DateTime());
            AddColumn("dbo.Houses", "NriRentFact", c => c.DateTime());
            AddColumn("dbo.Houses", "Updated", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Houses", "Updated");
            DropColumn("dbo.Houses", "NriRentFact");
            DropColumn("dbo.Houses", "NriRentPlan");
        }
    }
}
