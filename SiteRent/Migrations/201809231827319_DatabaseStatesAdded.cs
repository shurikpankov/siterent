namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseStatesAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DatabaseStates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        State = c.String(),
                        Progress = c.Int(nullable: false),
                        Success = c.Boolean(nullable: false),
                        SourceDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DatabaseStates");
        }
    }
}
