namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedIsPlannedBool : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Houses", "IsPersonal", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Houses", "IsPersonal");
        }
    }
}
