namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LatLongRecreated : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Houses", "Latitude", c => c.Decimal(precision: 18, scale: 15));
            AddColumn("dbo.Houses", "Longitude", c => c.Decimal(precision: 18, scale: 15));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Houses", "Longitude");
            DropColumn("dbo.Houses", "Latitude");
        }
    }
}
