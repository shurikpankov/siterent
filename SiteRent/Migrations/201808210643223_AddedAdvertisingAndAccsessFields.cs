namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAdvertisingAndAccsessFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Houses", "HasContractAdvertising", c => c.Boolean(nullable: false));
            AddColumn("dbo.Houses", "HasContractAdvancedAccess", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Houses", "HasContractAdvancedAccess");
            DropColumn("dbo.Houses", "HasContractAdvertising");
        }
    }
}
