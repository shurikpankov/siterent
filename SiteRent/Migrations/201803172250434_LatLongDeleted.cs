namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LatLongDeleted : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Houses", "Latitude");
            DropColumn("dbo.Houses", "Longitude");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Houses", "Longitude", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.Houses", "Latitude", c => c.Decimal(precision: 18, scale: 2));
        }
    }
}
