namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMasterPlanAndReports : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasterPlans",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(),
                        Type = c.String(),
                        Year = c.Int(nullable: false),
                        Period = c.Int(nullable: false),
                        Region = c.String(),
                        Filial = c.String(),
                        EmployeeId = c.Int(),
                        Value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(),
                        Type = c.String(),
                        Year = c.Int(nullable: false),
                        Period = c.Int(nullable: false),
                        Region = c.String(),
                        Filial = c.String(),
                        EmployeeId = c.Int(),
                        Value = c.Double(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Reports");
            DropTable("dbo.MasterPlans");
        }
    }
}
