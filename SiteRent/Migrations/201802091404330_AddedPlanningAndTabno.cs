namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPlanningAndTabno : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Houses", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Houses", new[] { "EmployeeId" });
            AddColumn("dbo.Employees", "TabNo", c => c.String());
            AddColumn("dbo.Houses", "IsPlanned", c => c.Boolean(nullable: false));
            AddColumn("dbo.Houses", "FrcApproval", c => c.DateTime());
            AddColumn("dbo.Houses", "FACT", c => c.DateTime());
            AddColumn("dbo.Houses", "FactDocumentDate", c => c.DateTime());
            AddColumn("dbo.Houses", "FactDocumentLink", c => c.String());
            AddColumn("dbo.Houses", "FactDocumentDescription", c => c.String());
            AddColumn("dbo.Houses", "IsApproved", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Houses", "EmployeeId", c => c.Int());
            CreateIndex("dbo.Houses", "EmployeeId");
            AddForeignKey("dbo.Houses", "EmployeeId", "dbo.Employees", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Houses", "EmployeeId", "dbo.Employees");
            DropIndex("dbo.Houses", new[] { "EmployeeId" });
            AlterColumn("dbo.Houses", "EmployeeId", c => c.Int(nullable: false));
            DropColumn("dbo.Houses", "IsApproved");
            DropColumn("dbo.Houses", "FactDocumentDescription");
            DropColumn("dbo.Houses", "FactDocumentLink");
            DropColumn("dbo.Houses", "FactDocumentDate");
            DropColumn("dbo.Houses", "FACT");
            DropColumn("dbo.Houses", "FrcApproval");
            DropColumn("dbo.Houses", "IsPlanned");
            DropColumn("dbo.Employees", "TabNo");
            CreateIndex("dbo.Houses", "EmployeeId");
            AddForeignKey("dbo.Houses", "EmployeeId", "dbo.Employees", "Id", cascadeDelete: true);
        }
    }
}
