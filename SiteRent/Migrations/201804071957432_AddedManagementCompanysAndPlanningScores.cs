namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedManagementCompanysAndPlanningScores : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ManagementCompanys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Region = c.String(),
                        Filial = c.String(),
                        CityId = c.Int(nullable: false),
                        City = c.String(),
                        Name = c.String(),
                        Contacts = c.String(),
                        RO18Houses = c.Int(nullable: false),
                        RO19Houses = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Houses", "PlanningScore", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Houses", "PlanningScore");
            DropTable("dbo.ManagementCompanys");
        }
    }
}
