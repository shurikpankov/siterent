namespace SiteRent.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCanApproveField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "CanApproveHouses", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "CanApproveHouses");
        }
    }
}
