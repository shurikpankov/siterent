﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace DbUpdater
{
    class Program
    {
        public static string ver = "2.2.0 23/09/2018";
        public static string connString = @"Data Source=MS-SQL009\SQL09;Initial Catalog=siterentdb;User Id=SiteRentApplication;Password=rD7PG5deYVz8SzH*$W?03hML";
        public static Dictionary<string, string> DataSourcesDictionary = new Dictionary<string, string>();
        public static Dictionary<string, bool> CheckList = new Dictionary<string, bool>();

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        public class Log
        {
            public int hId;
            public DateTime? GetHouseDate;
            public DateTime? FactAgreed;
            public DateTime? FactApproved;
            public DateTime? REFRESHDATE;
        }

        public class Doc
        {
            public int hId;
            public DateTime? CreationDate;
            public String Link;
            public String Description;
            public String DocId;
        }

        public class ManagementCompany
        {
            public int Id { get; set; }
            public string Region { get; set; }
            public string Filial { get; set; }
            public int CityId { get; set; }
            public string City { get; set; }

            public string Name { get; set; }
            public string Contacts { get; set; }

            public int RO18Houses { get; set; }
            public int RO19Houses { get; set; }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Loaded v" + ver);

            var handle = GetConsoleWindow();

            /*
            // Hide
            ShowWindow(handle, SW_HIDE);

            // Show
            ShowWindow(handle, SW_SHOW);
            */

            if (File.Exists(Directory.GetCurrentDirectory() + @"\datasources.txt"))
            {
                using (StreamReader srDatasources = new StreamReader(Directory.GetCurrentDirectory() + @"\datasources.txt"))
                {
                    String line;
                    String source;
                    String name;
                    while (!srDatasources.EndOfStream)
                    {
                        line = srDatasources.ReadLine();
                        if (!String.IsNullOrEmpty(line) && line.IndexOf('|') > -1)
                        {
                            source = line.Split('|')[0];
                            name = line.Split('|')[1];
                            if (!String.IsNullOrEmpty(source) && !String.IsNullOrEmpty(name))
                            {
                                DataSourcesDictionary.Add(name, source);
                            }
                        }
                    }
                }
            }
            else
            {
                CheckList.Add("datasource exists", false);
                //SaveLog();
            }

            foreach (var fileSource in DataSourcesDictionary)
            {
                bool HasCurrentFile = File.Exists(DataSourcesDictionary[fileSource.Key]);

                if (HasCurrentFile)
                {
                    string path1 = DataSourcesDictionary[fileSource.Key];
                    string path2 = Directory.GetCurrentDirectory() + @"\" + fileSource.Key + ".txt";
                    FileInfo fi1 = new FileInfo(path1);
                    FileInfo fi2 = new FileInfo(path2);
                    try
                    {
                        // Create the source file.
                        //using (FileStream fs = fi1.Create()) { }

                        //Ensure that the target file does not exist.
                        if (File.Exists(path2))
                        {
                            fi2.Delete();
                        }

                        //Copy the file.f
                        fi1.CopyTo(path2);
                        Console.WriteLine("{0} was copied to {1}.", path1, path2);
                    }
                    catch (IOException ioex)
                    {
                        Console.WriteLine(ioex.Message);
                        CheckList.Add(fileSource.Key + " copying", false);
                    }
                }
                else
                {
                    CheckList.Add(fileSource.Key + " exists", false);
                }
            }

            if (CheckList.Values.Contains(false))
            {
                //SaveLog();

                //Exit from application
                Environment.Exit(0);
            }

            bool HasEmployeesFile = File.Exists(Directory.GetCurrentDirectory() + @"\_NRI_Users_Rent.txt");            
            bool HasHousesFile = File.Exists(Directory.GetCurrentDirectory() + @"\_NRI.txt");
            bool HasLogFile = File.Exists(Directory.GetCurrentDirectory() + @"\_NRI_Logs_house_change_state.txt");
            bool HasDocFile = File.Exists(Directory.GetCurrentDirectory() + @"\_NRI_Documents.txt");
            bool HasDocHouseFile = File.Exists(Directory.GetCurrentDirectory() + @"\_NRI_Documents_Houses.txt");            
            bool HasEcmDocumentsFile = File.Exists(Directory.GetCurrentDirectory() + @"\_ECM_Contracts.txt");
            bool HasArfsFile = File.Exists(Directory.GetCurrentDirectory() + @"\_HD_aRFS.txt");
            bool HasConnectionStringFile = File.Exists(Directory.GetCurrentDirectory() + @"\_CONNECTION.txt");
            bool ConnectionSuccess = false;

            DateTime EmployeesDate = (HasEmployeesFile) ? File.GetLastWriteTime(Directory.GetCurrentDirectory() + @"\_NRI_Users_Rent.txt") : DateTime.Now;
            string EmployeeFileState = (HasEmployeesFile) ? "Employees (_NRI_Users_Rent.txt): OK " + EmployeesDate.ToString() : "Employees (_NRI_Users_Rent.txt): NOT FOUND";
            DateTime HousesDate = (HasHousesFile) ? File.GetLastWriteTime(Directory.GetCurrentDirectory() + @"\_NRI.txt") : DateTime.Now;
            string HousesFileState = (HasHousesFile) ? "Houses (_NRI.txt): OK " + HousesDate.ToString() : "Houses (_NRI.txt): NOT FOUND";
            DateTime LogsDate = (HasLogFile) ? File.GetLastWriteTime(Directory.GetCurrentDirectory() + @"\_NRI_Logs_house_change_state.txt") : DateTime.Now;
            string LogFileState = (HasLogFile) ? "Logs (_NRI_Logs_house_change_state.txt): OK " + LogsDate.ToString() : "Logs (_NRI_Logs_house_change_state.txt): NOT FOUND";
            DateTime DocsDate = (HasDocFile) ? File.GetLastWriteTime(Directory.GetCurrentDirectory() + @"\_NRI_Documents.txt") : DateTime.Now;
            string DocFileState = (HasDocFile) ? "Docs (_NRI_Documents.txt): OK " + DocsDate.ToString() : "Docs (_NRI_Documents.txt): NOT FOUND";
            DateTime DocsHouseDate = (HasDocHouseFile) ? File.GetLastWriteTime(Directory.GetCurrentDirectory() + @"\_NRI_Documents_Houses.txt") : DateTime.Now;
            string DocHouseFileState = (HasDocHouseFile) ? "Docs (_NRI_Documents_Houses.txt): OK " + DocsHouseDate.ToString() : "DocsHouses (_NRI_Documents_Houses.txt): NOT FOUND";
            DateTime EcmDocumentsDate = (HasEcmDocumentsFile) ? File.GetLastWriteTime(Directory.GetCurrentDirectory() + @"\_ECM_Contracts.txt") : DateTime.Now;
            string EcmDocumentsFileState = (HasEcmDocumentsFile) ? "ECM Documents (_ECM_Contracts.txt): OK " + EcmDocumentsDate.ToString() : "ECM Documents (_ECM_Contracts.txt): NOT FOUND";
            DateTime ArfsDate = (HasArfsFile) ? File.GetLastWriteTime(Directory.GetCurrentDirectory() + @"\_HD_aRFS.txt") : DateTime.Now;
            string ArfsFileState = (HasArfsFile) ? "aRFS (_HD_aRFS.txt): OK " + ArfsDate.ToString() : "aRFS (_HD_aRFS.txt): NOT FOUND";
            string ConnectionFileState = (HasConnectionStringFile) ? "Connection (_CONNECTION.txt): OK" : "Connection (_CONNECTION.txt): NOT FOUND";

            bool EmployeesData = true;
            bool HouseData = true;
            bool LogsData = true;
            bool DocsData = true;
            bool EcmDocumentsData = true;
            bool ArfsData = true;

            List<String> EmployeeDataList = new List<String>();
            List<String> LogsDataList = new List<String>();
            List<String> HousesDataList = new List<String>();
            List<String> ContractsDataList = new List<String>();
            List<String> ArfsDataList = new List<String>();


            List<String> EmployeeSqlIds = new List<String>();
            List<int> EmployeeDbIds = new List<int>();
            List<String> EmployeeDbFIO = new List<String>();
            List<String> HouseSqlIds = new List<String>();

            List<Log> Logs = new List<Log>();
            List<int> LogsIds = new List<int>();
            List<Doc> Docs = new List<Doc>();
            List<int> DocIds = new List<int>();
            List<Tuple<int, int, DateTime>> DocsHousesList = new List<Tuple<int, int, DateTime>>();

            List<String> ContractsDataIds = new List<String>();
            List<String> ContractsExistingDataIds = new List<String>();
            List<String> ECMContractsAtHouses = new List<String>();

            //int defaultReasponsibleId = -1;

            int InsertedEmployees = 0;
            int InsertedHouses = 0;
            int UpdatedEmployees = 0;
            int UpdatedHouses = 0;
            int BadHouses = 0;
            int UpdatedEcmDocuments = 0;
            int InsertedEcmDocuments = 0;

            if (HasConnectionStringFile)
            {
                using (StreamReader srConn = new StreamReader(Directory.GetCurrentDirectory() + @"\_CONNECTION.txt"))
                {
                    connString = srConn.ReadLine();
                }                
            }

            Console.WriteLine(EmployeeFileState);
            Console.WriteLine(HousesFileState);
            Console.WriteLine(LogFileState);
            Console.WriteLine(DocFileState);
            Console.WriteLine(ConnectionFileState);
            Console.WriteLine("Connecting to: " + connString.Replace("Password", "Pass..#").Split('#')[0]);

            using (SqlConnection connection = new SqlConnection(connString))
            {
                try
                {
                    connection.Open();
                }
                catch (Exception excp)
                {
                    Console.WriteLine("Connection test: UNSUCCESS");
                    Console.WriteLine(excp.ToString());
                }
                finally
                {
                    connection.Close();
                    ConnectionSuccess = true;
                    Console.WriteLine("Connection test: SUCCESS");
                }

            }

            if (HasEmployeesFile && HasHousesFile && HasConnectionStringFile && ConnectionSuccess)
            {
                //Console.ReadLine();

                #region CHECK_EMPLOYEES_FILE
                Console.Clear();
                Console.WriteLine("Checking Employees data...");
                using (StreamReader srEmployees = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Users_Rent.txt", Encoding.GetEncoding(1251)))
                {
                    string EmployeesHeader = srEmployees.ReadLine();

                    string e0 = "ID";
                    string e1 = "Логин";
                    string e2 = "Фамилия";
                    string e3 = "Имя";
                    string e4 = "Отчество";
                    string e5 = "EMAIL";
                    string e6 = "Роль";
                    string e7 = "Регионы";
                    string e8 = "Филиалы";

                    if (EmployeesHeader.Split(';')[0] == e0)
                    {
                        Console.WriteLine(e0 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e0 + ": NOT OK (" + EmployeesHeader.Split(';')[0] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[1] == e1)
                    {
                        Console.WriteLine(e1 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e1 + ": NOT OK (" + EmployeesHeader.Split(';')[1] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[2] == e2)
                    {
                        Console.WriteLine(e2 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e2 + ": NOT OK (" + EmployeesHeader.Split(';')[2] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[3] == e3)
                    {
                        Console.WriteLine(e3 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e3 + ": NOT OK (" + EmployeesHeader.Split(';')[3] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[4] == e4)
                    {
                        Console.WriteLine(e4 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e4 + ": NOT OK (" + EmployeesHeader.Split(';')[4] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[5] == e5)
                    {
                        Console.WriteLine(e5 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e5 + ": NOT OK (" + EmployeesHeader.Split(';')[5] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[6] == e6)
                    {
                        Console.WriteLine(e6 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e6 + ": NOT OK (" + EmployeesHeader.Split(';')[6] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[7] == e7)
                    {
                        Console.WriteLine(e7 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e7 + ": NOT OK (" + EmployeesHeader.Split(';')[7] + ")");
                        EmployeesData = false;
                    }

                    if (EmployeesHeader.Split(';')[8] == e8)
                    {
                        Console.WriteLine(e8 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e8 + ": NOT OK (" + EmployeesHeader.Split(';')[8] + ")");
                        EmployeesData = false;
                    }
                }
                if (EmployeesData)
                {
                    Console.WriteLine("Employees data checked: OK");
                }
                else
                {
                    Console.WriteLine("Employees data checked: NOT OK");
                }
                //Console.ReadLine();
                #endregion CHECK_EMPLOYEES_FILE

                #region CHECK_LOGS_FILE
                Console.Clear();
                Console.WriteLine("Checking Logs data...");
                using (StreamReader srLogs = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Logs_house_change_state.txt", Encoding.GetEncoding(1251)))
                {
                    string LogsHeader = srLogs.ReadLine();

                    string e0 = "ID";
                    string e1 = "TRACKABLE_ID";
                    string e2 = "TRACKABLE_TYPE";
                    string e8 = "KEY";
                    string e18 = "CREATED_AT";
                    string e24 = "NAME_OLD";
                    string e25 = "NAME_NUMBER";

                    if (LogsHeader.Split(';')[0] == e0)
                    {
                        Console.WriteLine(e0 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e0 + ": NOT OK (" + LogsHeader.Split(';')[0] + ")");
                        LogsData = false;
                    }

                    if (LogsHeader.Split(';')[1] == e1)
                    {
                        Console.WriteLine(e1 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e1 + ": NOT OK (" + LogsHeader.Split(';')[1] + ")");
                        LogsData = false;
                    }

                    if (LogsHeader.Split(';')[2] == e2)
                    {
                        Console.WriteLine(e2 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e2 + ": NOT OK (" + LogsHeader.Split(';')[2] + ")");
                        LogsData = false;
                    }

                    if (LogsHeader.Split(';')[8] == e8)
                    {
                        Console.WriteLine(e8 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e8 + ": NOT OK (" + LogsHeader.Split(';')[8] + ")");
                        LogsData = false;
                    }

                    if (LogsHeader.Split(';')[18] == e18)
                    {
                        Console.WriteLine(e18 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e18 + ": NOT OK (" + LogsHeader.Split(';')[18] + ")");
                        LogsData = false;
                    }

                    if (LogsHeader.Split(';')[24] == e24)
                    {
                        Console.WriteLine(e24 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e24 + ": NOT OK (" + LogsHeader.Split(';')[24] + ")");
                        LogsData = false;
                    }

                    if (LogsHeader.Split(';')[25] == e25)
                    {
                        Console.WriteLine(e25 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e25 + ": NOT OK (" + LogsHeader.Split(';')[25] + ")");
                        LogsData = false;
                    }
                }
                if (LogsData)
                {
                    Console.WriteLine("Logs data checked: OK");
                }
                else
                {
                    Console.WriteLine("Logs data checked: NOT OK");
                }
                //Console.ReadLine();
                #endregion CHECK_LOGS_FILE

                #region CHECK_Docs_FILES
                Console.Clear();
                Console.WriteLine("Checking Docs data...");
                using (StreamReader srDocs = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Documents.txt", Encoding.GetEncoding(1251)))
                {
                    string DocsHeader = srDocs.ReadLine();

                    string e0 = "ID";
                    string e1 = "HOUSE_DOCUMENT_TYPE_ID";
                    string e3 = "LINK";
                    string e4 = "DESCRIPTION";
                    string e5 = "CREATED_AT";                    

                    if (DocsHeader.Split(';')[0] == e0)
                    {
                        Console.WriteLine(e0 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e0 + ": NOT OK (" + DocsHeader.Split(';')[0] + ")");
                        DocsData = false;
                    }

                    if (DocsHeader.Split(';')[1] == e1)
                    {
                        Console.WriteLine(e1 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e1 + ": NOT OK (" + DocsHeader.Split(';')[1] + ")");
                        DocsData = false;
                    }

                    if (DocsHeader.Split(';')[3] == e3)
                    {
                        Console.WriteLine(e3 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e3 + ": NOT OK (" + DocsHeader.Split(';')[3] + ")");
                        DocsData = false;
                    }

                    if (DocsHeader.Split(';')[4] == e4)
                    {
                        Console.WriteLine(e4 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e4 + ": NOT OK (" + DocsHeader.Split(';')[4] + ")");
                        DocsData = false;
                    }

                    if (DocsHeader.Split(';')[5] == e5)
                    {
                        Console.WriteLine(e5 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e5 + ": NOT OK (" + DocsHeader.Split(';')[5] + ")");
                        DocsData = false;
                    }
                }
                if (DocsData)
                {
                    Console.WriteLine("Docs data checked: OK");
                }
                else
                {
                    Console.WriteLine("Docs data checked: NOT OK");
                }


                Console.WriteLine("Checking DocsHouses data...");
                using (StreamReader srDocshouses = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Documents_Houses.txt", Encoding.GetEncoding(1251)))
                {
                    string DocsHeader = srDocshouses.ReadLine();

                    string e0 = "DOCUMENT_ID";
                    string e1 = "HOUSE_ID";

                    if (DocsHeader.Split(';')[0] == e0)
                    {
                        Console.WriteLine(e0 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e0 + ": NOT OK (" + DocsHeader.Split(';')[0] + ")");
                        DocsData = false;
                    }

                    if (DocsHeader.Split(';')[1] == e1)
                    {
                        Console.WriteLine(e1 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e1 + ": NOT OK (" + DocsHeader.Split(';')[1] + ")");
                        DocsData = false;
                    }                    
                }
                if (DocsData)
                {
                    Console.WriteLine("DocsHouses data checked: OK");
                }
                else
                {
                    Console.WriteLine("DocsHouses data checked: NOT OK");
                }
                
                //Console.ReadLine();
                #endregion CHECK_Docs_FILES

                #region CHECK_HOUSES_FILE
                Console.Clear();
                Console.WriteLine("Checking Houses data...");
                using (StreamReader srHouses = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI.txt", Encoding.GetEncoding(1251)))
                {
                    string HousesHeader = srHouses.ReadLine();

                    string h0 = "ИдДома"; //NriId
                    if (HousesHeader.Split(';')[0] == h0)
                    {
                        Console.WriteLine(h0 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h0 + ": NOT OK (" + HousesHeader.Split(';')[0] + ")");
                        HouseData = false;
                    }

                    string h3 = "Регион"; //Region
                    if (HousesHeader.Split(';')[3] == h3)
                    {
                        Console.WriteLine(h3 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h3 + ": NOT OK (" + HousesHeader.Split(';')[3] + ")");
                        HouseData = false;
                    }

                    string h4 = "Филиал"; //Filial
                    if (HousesHeader.Split(';')[4] == h4)
                    {
                        Console.WriteLine(h4 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h4 + ": NOT OK (" + HousesHeader.Split(';')[4] + ")");
                        HouseData = false;
                    }

                    string h5 = "ИдГорода"; //CityId
                    if (HousesHeader.Split(';')[5] == h5)
                    {
                        Console.WriteLine(h5 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h5 + ": NOT OK (" + HousesHeader.Split(';')[5] + ")");
                        HouseData = false;
                    }

                    string h6 = "Город"; //City
                    if (HousesHeader.Split(';')[6] == h6)
                    {
                        Console.WriteLine(h6 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h6 + ": NOT OK (" + HousesHeader.Split(';')[6] + ")");
                        HouseData = false;
                    }

                    string h7 = "ИдРайона"; //AreaId
                    if (HousesHeader.Split(';')[7] == h7)
                    {
                        Console.WriteLine(h7 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h7 + ": NOT OK (" + HousesHeader.Split(';')[7] + ")");
                        HouseData = false;
                    }

                    string h8 = "Район"; //Area
                    if (HousesHeader.Split(';')[8] == h8)
                    {
                        Console.WriteLine(h8 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h8 + ": NOT OK (" + HousesHeader.Split(';')[8] + ")");
                        HouseData = false;
                    }

                    string h9 = "Улица"; //StreetName
                    if (HousesHeader.Split(';')[9] == h9)
                    {
                        Console.WriteLine(h9 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h9 + ": NOT OK (" + HousesHeader.Split(';')[9] + ")");
                        HouseData = false;
                    }

                    string h10 = "Тип"; //StreetType
                    if (HousesHeader.Split(';')[10] == h10)
                    {
                        Console.WriteLine(h10 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h10 + ": NOT OK (" + HousesHeader.Split(';')[10] + ")");
                        HouseData = false;
                    }

                    string h11 = "NДом1"; //Number1
                    if (HousesHeader.Split(';')[11] == h11)
                    {
                        Console.WriteLine(h11 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h11 + ": NOT OK (" + HousesHeader.Split(';')[11] + ")");
                        HouseData = false;
                    }

                    string h12 = "ТипNДом1"; //Type1
                    if (HousesHeader.Split(';')[12] == h12)
                    {
                        Console.WriteLine(h12 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h12 + ": NOT OK (" + HousesHeader.Split(';')[12] + ")");
                        HouseData = false;
                    }

                    string h13 = "NДом2"; //Number2
                    if (HousesHeader.Split(';')[13] == h13)
                    {
                        Console.WriteLine(h13 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h13 + ": NOT OK (" + HousesHeader.Split(';')[13] + ")");
                        HouseData = false;
                    }

                    string h14 = "ТипNДом2"; //Type2
                    if (HousesHeader.Split(';')[14] == h14)
                    {
                        Console.WriteLine(h14 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h14 + ": NOT OK (" + HousesHeader.Split(';')[14] + ")");
                        HouseData = false;
                    }

                    string h15 = "NДом3"; //Number3
                    if (HousesHeader.Split(';')[15] == h15)
                    {
                        Console.WriteLine(h15 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h15 + ": NOT OK (" + HousesHeader.Split(';')[15] + ")");
                        HouseData = false;
                    }

                    string h16 = "ТипNДом3"; //Type3
                    if (HousesHeader.Split(';')[16] == h16)
                    {
                        Console.WriteLine(h16 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h16 + ": NOT OK (" + HousesHeader.Split(';')[16] + ")");
                        HouseData = false;
                    }

                    string h17 = "ГФК"; //GFK
                    if (HousesHeader.Split(';')[17] == h17)
                    {
                        Console.WriteLine(h17 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h17 + ": NOT OK (" + HousesHeader.Split(';')[17] + ")");
                        HouseData = false;
                    }

                    string h18 = "Код ЕРП"; //ERP
                    if (HousesHeader.Split(';')[18] == h18)
                    {
                        Console.WriteLine(h18 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h18 + ": NOT OK (" + HousesHeader.Split(';')[18] + ")");
                        HouseData = false;
                    }

                    string h63 = "ПланДтЗавершенияАктив"; //RoYear
                    if (HousesHeader.Split(';')[63] == h63)
                    {
                        Console.WriteLine(h63 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h63 + ": NOT OK (" + HousesHeader.Split(';')[63] + ")");
                        HouseData = false;
                    }

                    string h19 = "Статус"; //Status
                    if (HousesHeader.Split(';')[19] == h19)
                    {
                        Console.WriteLine(h19 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h19 + ": NOT OK (" + HousesHeader.Split(';')[19] + ")");
                        HouseData = false;
                    }

                    string h26 = "Квартиры"; //Flats
                    if (HousesHeader.Split(';')[26] == h26)
                    {
                        Console.WriteLine(h26 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h26 + ": NOT OK (" + HousesHeader.Split(';')[26] + ")");
                        HouseData = false;
                    }

                    string h25 = "Этажи"; //Floors
                    if (HousesHeader.Split(';')[25] == h25)
                    {
                        Console.WriteLine(h25 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h25 + ": NOT OK (" + HousesHeader.Split(';')[25] + ")");
                        HouseData = false;
                    }

                    string h24 = "Подъезды"; //Entrances
                    if (HousesHeader.Split(';')[24] == h24)
                    {
                        Console.WriteLine(h24 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h24 + ": NOT OK (" + HousesHeader.Split(';')[24] + ")");
                        HouseData = false;
                    }

                    string h29 = "СтоимСоглас"; //PlanAgreementPrice
                    if (HousesHeader.Split(';')[29] == h29)
                    {
                        Console.WriteLine(h29 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h29 + ": NOT OK (" + HousesHeader.Split(';')[29] + ")");
                        HouseData = false;
                    }

                    string h30 = "СтоимАрендыМесяц"; //PlanRentPrice
                    if (HousesHeader.Split(';')[30] == h30)
                    {
                        Console.WriteLine(h30 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h30 + ": NOT OK (" + HousesHeader.Split(';')[30] + ")");
                        HouseData = false;
                    }

                    string h66 = "НаимКонтрАг"; //UK
                    if (HousesHeader.Split(';')[66] == h66)
                    {
                        Console.WriteLine(h66 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h66 + ": NOT OK (" + HousesHeader.Split(';')[66] + ")");
                        HouseData = false;
                    }

                    string h67 = "КонтактДанныеКонтрАг"; //UkContacts
                    if (HousesHeader.Split(';')[67] == h67)
                    {
                        Console.WriteLine(h67 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h67 + ": NOT OK (" + HousesHeader.Split(';')[67] + ")");
                        HouseData = false;
                    }

                    string h68 = "ТребПротоколыСобрСобств"; //NeedOss
                    if (HousesHeader.Split(';')[68] == h68)
                    {
                        Console.WriteLine(h68 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h68 + ": NOT OK (" + HousesHeader.Split(';')[68] + ")");
                        HouseData = false;
                    }

                    string h69 = "ТребПредпроект"; //NeedPir
                    if (HousesHeader.Split(';')[69] == h69)
                    {
                        Console.WriteLine(h69 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h69 + ": NOT OK (" + HousesHeader.Split(';')[69] + ")");
                        HouseData = false;
                    }

                    string h21 = "Коммент"; //Comment
                    if (HousesHeader.Split(';')[21] == h21)
                    {
                        Console.WriteLine(h21 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h21 + ": NOT OK (" + HousesHeader.Split(';')[21] + ")");
                        HouseData = false;
                    }

                    string h65 = "ФИО согласователя"; //*Ответственный*
                    if (HousesHeader.Split(';')[65] == h65)
                    {
                        Console.WriteLine(h65 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h65 + ": NOT OK (" + HousesHeader.Split(';')[65] + ")");
                        HouseData = false;
                    }

                    string h96 = "ПланДтСогласование"; //*Плановая дата согласования в NRI*
                    if (HousesHeader.Split(';')[96] == h96)
                    {
                        Console.WriteLine(h96 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h96 + ": NOT OK (" + HousesHeader.Split(';')[96] + ")");
                        HouseData = false;
                    }

                    string h97 = "ФактДтСогласование"; //*Ответственный*
                    if (HousesHeader.Split(';')[97] == h97)
                    {
                        Console.WriteLine(h97 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(h97 + ": NOT OK (" + HousesHeader.Split(';')[97] + ")");
                        HouseData = false;
                    }

                }
                if (HouseData)
                {
                    Console.WriteLine("Houses data checked: OK");
                }
                else
                {
                    Console.WriteLine("Houses data checked: NOT OK");
                }
                #endregion CHECK_HOUSES_FILE

                #region CHECK_ECMCONTRACTS_FILE
                Console.Clear();
                Console.WriteLine("Checking ECM contracts data...");
                using (StreamReader srEcmContracts = new StreamReader(Directory.GetCurrentDirectory() + @"\_ECM_Contracts.txt", Encoding.GetEncoding(1251)))
                {
                    string EcmContractsHeader = srEcmContracts.ReadLine();

                    string e0 = "r_object_id";
                    string e6 = "Статус";
                    string e8 = "Тип документа";
                    string e12 = "Контрагент";
                    string e13 = "Инн контрагента";
                    string e22 = "Номер документа";
                    string e24 = "Номер договора";
                    string e25 = "Номер СЭВД";
                    string e26 = "Ссылка СЭВД";
                    string e27 = "Дата начала";
                    string e31 = "Сумма договора";
                    string e32 = "Валюта договора";
                    string e39 = "НДС";
                    string e50 = "Номер ЗП";
                    string e52 = "Системный номер";
                    string e64 = "Резюме";
                    string e83 = "Дата фактического подписания";
                    string e85 = "Ссылка на ЗП в НФС";


                    if (EcmContractsHeader.Split('\t')[0] == e0)
                    {
                        Console.WriteLine(e0 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e0 + ": NOT OK (" + EcmContractsHeader.Split('\t')[0] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[6] == e6)
                    {
                        Console.WriteLine(e6 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e6 + ": NOT OK (" + EcmContractsHeader.Split('\t')[6] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[8] == e8)
                    {
                        Console.WriteLine(e8 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e8 + ": NOT OK (" + EcmContractsHeader.Split('\t')[8] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[12] == e12)
                    {
                        Console.WriteLine(e12 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e12 + ": NOT OK (" + EcmContractsHeader.Split('\t')[12] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[13] == e13)
                    {
                        Console.WriteLine(e13 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e13 + ": NOT OK (" + EcmContractsHeader.Split('\t')[13] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[22] == e22)
                    {
                        Console.WriteLine(e22 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e22 + ": NOT OK (" + EcmContractsHeader.Split('\t')[22] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[24] == e24)
                    {
                        Console.WriteLine(e24 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e24 + ": NOT OK (" + EcmContractsHeader.Split('\t')[24] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[25] == e25)
                    {
                        Console.WriteLine(e25 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e25 + ": NOT OK (" + EcmContractsHeader.Split('\t')[25] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[26] == e26)
                    {
                        Console.WriteLine(e26 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e26 + ": NOT OK (" + EcmContractsHeader.Split('\t')[26] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[27] == e27)
                    {
                        Console.WriteLine(e27 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e27 + ": NOT OK (" + EcmContractsHeader.Split('\t')[27] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[31] == e31)
                    {
                        Console.WriteLine(e31 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e31 + ": NOT OK (" + EcmContractsHeader.Split('\t')[31] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[32] == e32)
                    {
                        Console.WriteLine(e32 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e32 + ": NOT OK (" + EcmContractsHeader.Split('\t')[32] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[39] == e39)
                    {
                        Console.WriteLine(e39 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e39 + ": NOT OK (" + EcmContractsHeader.Split('\t')[39] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[50] == e50)
                    {
                        Console.WriteLine(e50 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e50 + ": NOT OK (" + EcmContractsHeader.Split('\t')[50] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[52] == e52)
                    {
                        Console.WriteLine(e52 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e52 + ": NOT OK (" + EcmContractsHeader.Split('\t')[52] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[64] == e64)
                    {
                        Console.WriteLine(e64 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e64 + ": NOT OK (" + EcmContractsHeader.Split('\t')[64] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[83] == e83)
                    {
                        Console.WriteLine(e83 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e83 + ": NOT OK (" + EcmContractsHeader.Split('\t')[83] + ")");
                        EcmDocumentsData = false;
                    }

                    if (EcmContractsHeader.Split('\t')[85] == e85)
                    {
                        Console.WriteLine(e85 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e85 + ": NOT OK (" + EcmContractsHeader.Split('\t')[85] + ")");
                        EcmDocumentsData = false;
                    }
                }
                if (EcmDocumentsData)
                {
                    Console.WriteLine("ECM Contracts data checked: OK");
                }
                else
                {
                    Console.WriteLine("ECM Contracts data checked: NOT OK");
                }
                //Console.ReadLine();
                #endregion CHECK_ECMCONTRACTS_FILE

                #region CHECK_ARFS_FILES
                Console.Clear();
                Console.WriteLine("Checking Arfs data...");
                using (StreamReader srArfs = new StreamReader(Directory.GetCurrentDirectory() + @"\_HD_aRFS.txt", Encoding.GetEncoding(1251)))
                {
                    string ArfsHeader = srArfs.ReadLine();

                    string e1 = "Город";
                    string e9 = "Код НФС дома";
                    string e10 = "Подключения. Проблема с доступом";
                    string e11 = "Подключения. Нет договора с УК";
                    string e12 = "Подключения. Затопленный Подвал";
                    string e13 = "Подключения. Нет доступа в выходные дни или вечернее время";
                    string e14 = "Подключения. Согласование проекта для модернизации объекта";
                    string e15 = "Подключения. Доступ по алгоритму";

                    if (ArfsHeader.Split('\t')[1] == e1)
                    {
                        Console.WriteLine(e1 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e1 + ": NOT OK (" + ArfsHeader.Split('\t')[1] + ")");
                        ArfsData = false;
                    }

                    if (ArfsHeader.Split('\t')[9] == e9)
                    {
                        Console.WriteLine(e9 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e9 + ": NOT OK (" + ArfsHeader.Split('\t')[9] + ")");
                        ArfsData = false;
                    }

                    if (ArfsHeader.Split('\t')[10] == e10)
                    {
                        Console.WriteLine(e10 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e10 + ": NOT OK (" + ArfsHeader.Split('\t')[10] + ")");
                        ArfsData = false;
                    }

                    if (ArfsHeader.Split('\t')[11] == e11)
                    {
                        Console.WriteLine(e11 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e11 + ": NOT OK (" + ArfsHeader.Split('\t')[11] + ")");
                        ArfsData = false;
                    }

                    if (ArfsHeader.Split('\t')[12] == e12)
                    {
                        Console.WriteLine(e12 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e12 + ": NOT OK (" + ArfsHeader.Split('\t')[12] + ")");
                        ArfsData = false;
                    }

                    if (ArfsHeader.Split('\t')[13] == e13)
                    {
                        Console.WriteLine(e13 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e13 + ": NOT OK (" + ArfsHeader.Split('\t')[13] + ")");
                        ArfsData = false;
                    }

                    if (ArfsHeader.Split('\t')[14] == e14)
                    {
                        Console.WriteLine(e14 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e14 + ": NOT OK (" + ArfsHeader.Split('\t')[14] + ")");
                        ArfsData = false;
                    }

                    if (ArfsHeader.Split('\t')[15] == e15)
                    {
                        Console.WriteLine(e15 + ": OK");
                    }
                    else
                    {
                        Console.WriteLine(e15 + ": NOT OK (" + ArfsHeader.Split('\t')[15] + ")");
                        ArfsData = false;
                    }
                }
                if (ArfsData)
                {
                    Console.WriteLine("Arfs data checked: OK");
                }
                else
                {
                    Console.WriteLine("Arfs data checked: NOT OK");
                }
                #endregion CHECK_ARFS_FILES

                if (EmployeesData && LogsData && HouseData && DocsData && EcmDocumentsData && ArfsData)
                {
                    //Console.ReadLine();

                    Console.Clear();
                    Console.WriteLine("Reading Employees from file...");
                    using (StreamReader srEmployees = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Users_Rent.txt", Encoding.GetEncoding(1251)))
                    {
                        srEmployees.ReadLine();
                        while (!srEmployees.EndOfStream)
                        {
                            EmployeeDataList.Add(srEmployees.ReadLine());
                        }
                    }
                    Console.WriteLine("Found " + EmployeeDataList.Count.ToString() + " Employees in DataFile");

                    Console.WriteLine("Reading Logs from file...");
                    using (StreamReader srLogs = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Logs_house_change_state.txt", Encoding.GetEncoding(1251)))
                    {
                        srLogs.ReadLine();
                        while (!srLogs.EndOfStream)
                        {
                            string line = srLogs.ReadLine();
                            string strId = line.Split(';')[1];
                            int hId = -1;
                            if (int.TryParse(strId, out hId))
                            {
                                string TRACKABLE_TYPE = line.Split(';')[2];
                                string KEY = line.Split(';')[8];
                                if (TRACKABLE_TYPE == "House" && KEY == "house.change_state")
                                {
                                    string CREATED_AT = line.Split(';')[18];
                                    string NAME_OLD = line.Split(';')[24];
                                    string NAME_NUMBER = line.Split(';')[25];

                                    DateTime? GetHouseDate = null;
                                    DateTime? FactAgreed = null;
                                    DateTime? FactApproved = null;
                                    DateTime? REFRESHDATE = null;

                                    if (NAME_NUMBER == "Проработка невозможна")
                                    {
                                        DateTime dt = DateTime.Now;
                                        if (DateTime.TryParse(CREATED_AT, out dt))
                                        {
                                            REFRESHDATE = dt;
                                        }
                                    }

                                    if (NAME_OLD == "Проверка оценки окупаемости" && (NAME_NUMBER == "Согласование с УК" || NAME_NUMBER == "Подписание договора с УК"))
                                    {
                                        DateTime dt = DateTime.Now;
                                        if (DateTime.TryParse(CREATED_AT, out dt))
                                        {
                                            GetHouseDate = dt;
                                        }                                        
                                    }

                                    if (NAME_NUMBER == "Проверка доступа на дом")
                                    {
                                        DateTime dt = DateTime.Now;
                                        if (DateTime.TryParse(CREATED_AT, out dt))
                                        {
                                            FactAgreed = dt;
                                        }
                                    }

                                    if (NAME_NUMBER == "Проведение ПИР")
                                    {
                                        DateTime dt = DateTime.Now;
                                        if (DateTime.TryParse(CREATED_AT, out dt))
                                        {
                                            FactApproved = dt;
                                        }
                                    }

                                    int lInd = LogsIds.IndexOf(hId);
                                    if (lInd == -1)
                                    {
                                        LogsIds.Add(hId);
                                        Log lg = new Log();
                                        lg.hId = hId;
                                        lg.GetHouseDate = GetHouseDate;
                                        lg.FactAgreed = FactAgreed;
                                        lg.FactApproved = FactApproved;
                                        lg.REFRESHDATE = REFRESHDATE;
                                        Logs.Add(lg);
                                    }
                                    else
                                    {
                                        if ((REFRESHDATE != null && REFRESHDATE.HasValue && !Logs[lInd].REFRESHDATE.HasValue) || (REFRESHDATE != null && REFRESHDATE.HasValue && Logs[lInd].REFRESHDATE.HasValue && REFRESHDATE.Value > Logs[lInd].REFRESHDATE.Value))
                                        {
                                            Logs[lInd].REFRESHDATE = REFRESHDATE;
                                        }

                                        if ((GetHouseDate != null && GetHouseDate.HasValue && !Logs[lInd].GetHouseDate.HasValue) || (GetHouseDate != null && GetHouseDate.HasValue && Logs[lInd].GetHouseDate.HasValue && GetHouseDate.Value > Logs[lInd].GetHouseDate.Value))
                                        {
                                            Logs[lInd].GetHouseDate = GetHouseDate;
                                        }

                                        if ((FactAgreed != null && FactAgreed.HasValue && !Logs[lInd].FactAgreed.HasValue) || (FactAgreed != null && FactAgreed.HasValue && Logs[lInd].FactAgreed.HasValue && FactAgreed.Value > Logs[lInd].FactAgreed.Value))
                                        {
                                            Logs[lInd].FactAgreed = FactAgreed;
                                        }

                                        if ((FactApproved != null && FactApproved.HasValue && !Logs[lInd].FactApproved.HasValue) || (FactApproved != null && FactApproved.HasValue && Logs[lInd].FactApproved.HasValue && FactApproved.Value > Logs[lInd].FactApproved.Value))
                                        {
                                            Logs[lInd].FactApproved = FactApproved;
                                        }
                                    }
                                }                                
                            }
                        }
                    }
                    foreach (Log lg in Logs)
                    {
                        if (lg.REFRESHDATE.HasValue)
                        {
                            if (lg.FactAgreed.HasValue && lg.FactAgreed.Value < lg.REFRESHDATE.Value)
                                lg.FactAgreed = null;

                            if (lg.FactApproved.HasValue && lg.FactApproved.Value < lg.REFRESHDATE.Value)
                                lg.FactApproved = null;

                            if (lg.GetHouseDate.HasValue && lg.GetHouseDate.Value < lg.REFRESHDATE.Value)
                                lg.GetHouseDate = null;
                        }
                    }
                    Console.WriteLine("Found Logs on " + Logs.Count() + " houses in DataFile");

                    
                    Console.WriteLine("Reading Docs from files...");
                    using (StreamReader srDocsHouses = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Documents_Houses.txt", Encoding.GetEncoding(1251)))
                    {
                        srDocsHouses.ReadLine();
                        while (!srDocsHouses.EndOfStream)
                        {
                            string line = srDocsHouses.ReadLine();
                            string strId = line.Split(';')[1];
                            int hId = -1;
                            if (int.TryParse(strId, out hId) && hId > -1)
                            {
                                int dId = -1;
                                string strDoc = line.Split(';')[0];
                                if (int.TryParse(strDoc, out dId) && dId > -1)
                                {
                                    string strDate = line.Split(';')[2];
                                    DateTime docCreatedDT = DateTime.MinValue;
                                    if (DateTime.TryParse(strDate, out docCreatedDT))
                                    {
                                        DocsHousesList.Add(new Tuple<int, int, DateTime>(dId, hId, docCreatedDT));
                                    }                                    
                                }
                            }
                        }
                    }
                    Console.WriteLine("Found " + DocsHousesList.Count() + "Pairs in DocHouses DataFile");

                    using (StreamReader srDocs = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI_Documents.txt", Encoding.GetEncoding(1251)))
                    {
                        srDocs.ReadLine();
                        while (!srDocs.EndOfStream)
                        {
                            string line = srDocs.ReadLine();
                            string strId = line.Split(';')[0];
                            int dId = -1;
                            if (int.TryParse(strId, out dId) && dId > -1 && line.Split(';')[1] == "10009" && DocsHousesList.Where(x => x.Item1 == dId).Count() > 0)
                            {   
                                foreach (Tuple<int, int, DateTime> tpl in DocsHousesList.Where(x => x.Item1 == dId).ToList())
                                {
                                    Doc cDoc = new Doc();
                                    cDoc.hId = tpl.Item2;
                                    DateTime dtCreationDate = DateTime.Now;
                                    if (DateTime.TryParse(line.Split(';')[5], out dtCreationDate))
                                    {
                                        cDoc.CreationDate = dtCreationDate;
                                    }
                                    else
                                    {
                                        cDoc.CreationDate = null;
                                    }

                                    cDoc.Link = line.Split(';')[3];
                                    Regex rx = new Regex(@"[0-9a-fA-F]{16}");
                                    if (rx.IsMatch(cDoc.Link))
                                    {
                                        cDoc.DocId = rx.Match(cDoc.Link).Value;
                                    }
                                    else
                                    {
                                        cDoc.DocId = String.Empty;
                                    }
                                    cDoc.Description = line.Split(';')[4];

                                    if(cDoc.Link.Contains(@"http://ecm-bl01-prd.vimpelcom.ru") || cDoc.Link.Contains(@"https://ecm-bl01-prd.vimpelcom.ru") || cDoc.Link.Contains(@"sharepoint") || cDoc.Link.Contains(@"\\bee.vimpelcom.ru\folders\"))
                                    { 
                                        Docs.Add(cDoc);
                                        DocIds.Add(tpl.Item2);
                                    }
                                }                                                                 
                            }
                        }
                    }
                    Console.WriteLine("Found Docs on " + Docs.Count() + " houses in DataFile");
                    

                    Console.WriteLine("Reading Houses from file...");

                    using (StreamReader srHouses = new StreamReader(Directory.GetCurrentDirectory() + @"\_NRI.txt", Encoding.GetEncoding(1251)))
                    {
                        srHouses.ReadLine();
                        while (!srHouses.EndOfStream)
                        {
                            HousesDataList.Add(srHouses.ReadLine());
                        }
                    }
                    Console.WriteLine("Found " + HousesDataList.Count.ToString() + " Houses in DataFile");

                    Console.WriteLine("Reading ECM Documents from file...");
                    using (StreamReader srEcmDocuments = new StreamReader(Directory.GetCurrentDirectory() + @"\_ECM_Contracts.txt", Encoding.GetEncoding(1251)))
                    {
                        srEcmDocuments.ReadLine();
                        while (!srEcmDocuments.EndOfStream)
                        {
                            string docLine = srEcmDocuments.ReadLine();
                            Regex rx = new Regex(@"[0-9a-fA-F]{16}");
                            if(rx.IsMatch(docLine.Split('\t')[0]) && docLine.Split('\t').Count() >= 86)
                            { 
                                ContractsDataList.Add(docLine);
                                ContractsDataIds.Add(docLine.Split('\t')[0]);
                            }
                        }
                    }
                    Console.WriteLine("Found " + ContractsDataList.Count.ToString() + " ECM Documents in DataFile");

                    Console.WriteLine("Reading aRFS data from file...");
                    using (StreamReader srArfs = new StreamReader(Directory.GetCurrentDirectory() + @"\_HD_aRFS.txt", Encoding.GetEncoding(1251)))
                    {
                        srArfs.ReadLine();
                        while (!srArfs.EndOfStream)
                        {
                            ArfsDataList.Add(srArfs.ReadLine());
                        }
                    }
                    Console.WriteLine("Found " + ArfsDataList.Count.ToString() + " aRFS houses in DataFile");

                    using (SqlConnection connection = new SqlConnection(connString))
                    {
                        try
                        {
                            connection.Open();

                            //String qsUpdateDatabaseState = "UPDATE dbo.Employees SET Email = @email, Surname = @surname, Name = @name, Patronymic = @patronymic, Regions = @regions, Filials = @filials WHERE NriId = @nriId";

                            Console.WriteLine("Reading Employees from SQL...");
                            string qsEmployees = "SELECT NriId FROM dbo.Employees";
                            SqlCommand cmdSelectCurrentEmployees = new SqlCommand(qsEmployees, connection);
                                                        
                            SqlDataReader readerEmployees = cmdSelectCurrentEmployees.ExecuteReader();
                            while (readerEmployees.Read())
                            {
                                EmployeeSqlIds.Add(readerEmployees[0].ToString());
                            }
                            readerEmployees.Close();

                            Console.WriteLine("Found " + EmployeeSqlIds.Count.ToString() + " Employees in SQL");

                            Console.WriteLine("Reading Houses from SQL...");
                            string qsHouses = "SELECT NriId FROM dbo.Houses";
                            SqlCommand cmdSelectCurrentHouses = new SqlCommand(qsHouses, connection);

                            SqlDataReader readerHouses = cmdSelectCurrentHouses.ExecuteReader();
                            while (readerHouses.Read())
                            {
                                HouseSqlIds.Add(readerHouses[0].ToString());
                            }
                            readerHouses.Close();

                            Console.WriteLine("Found " + HouseSqlIds.Count.ToString() + " Houses in SQL");

                            //START EMPLOYEE SQL UPDATE
                            Console.WriteLine("Updating Employees data..");

                            String qsInsertEmployees = "INSERT INTO dbo.Employees([NriId],[Email],[Surname],[Name],[Patronymic],[Regions],[Filials],[CanApproveHouses]) VALUES (@nriId, @email, @surname, @name, @patronymic, @regions, @filials, @canApproveHouses)";
                            String qsUpdateEmployees = "UPDATE dbo.Employees SET Email = @email, Surname = @surname, Name = @name, Patronymic = @patronymic, Regions = @regions, Filials = @filials WHERE NriId = @nriId";

                            foreach (string currEmployee in EmployeeDataList)
                            {
                                string cId = currEmployee.Split(';')[0];
                                if (EmployeeSqlIds.IndexOf(cId) > -1)
                                {
                                    //UPDATE
                                    SqlCommand updCommand = new SqlCommand(qsUpdateEmployees, connection);
                                    updCommand.Parameters.AddWithValue("@nriId", Convert.ToInt32(cId));
                                    updCommand.Parameters.AddWithValue("@email", currEmployee.Split(';')[5]);
                                    updCommand.Parameters.AddWithValue("@surname", currEmployee.Split(';')[2]);
                                    updCommand.Parameters.AddWithValue("@name", currEmployee.Split(';')[3]);
                                    updCommand.Parameters.AddWithValue("@patronymic", currEmployee.Split(';')[4]);
                                    updCommand.Parameters.AddWithValue("@regions", currEmployee.Split(';')[7]);
                                    updCommand.Parameters.AddWithValue("@filials", currEmployee.Split(';')[8]);
                                    updCommand.ExecuteNonQuery();
                                    UpdatedEmployees += 1;
                                }
                                else
                                {
                                    //INSERT
                                    SqlCommand insCommand = new SqlCommand(qsInsertEmployees, connection);
                                    insCommand.Parameters.AddWithValue("@nriId", Convert.ToInt32(cId));
                                    insCommand.Parameters.AddWithValue("@email", currEmployee.Split(';')[5]);
                                    insCommand.Parameters.AddWithValue("@surname", currEmployee.Split(';')[2]);
                                    insCommand.Parameters.AddWithValue("@name", currEmployee.Split(';')[3]);
                                    insCommand.Parameters.AddWithValue("@patronymic", currEmployee.Split(';')[4]);
                                    insCommand.Parameters.AddWithValue("@regions", currEmployee.Split(';')[7]);
                                    insCommand.Parameters.AddWithValue("@filials", currEmployee.Split(';')[8]);
                                    insCommand.Parameters.AddWithValue("@canApproveHouses", false);
                                    insCommand.ExecuteNonQuery();
                                    InsertedEmployees += 1;
                                }
                            }


                            //START HOUSE SQL UPDATE
                            

                            //FILL CURR EMPLOYEES - UPDATE AFTER INSERT

                            string qsNewEmployees = "SELECT [Id], [Surname], [Name], [Patronymic] FROM dbo.Employees";
                            SqlCommand cmdSelectNewDataAboutEmployees = new SqlCommand(qsNewEmployees, connection);

                            SqlDataReader readerNewDataAboutEmployees = cmdSelectNewDataAboutEmployees.ExecuteReader();
                            while (readerNewDataAboutEmployees.Read())
                            {
                                EmployeeDbIds.Add(Convert.ToInt32(readerNewDataAboutEmployees[0]));
                                EmployeeDbFIO.Add(readerNewDataAboutEmployees[1].ToString() + " " + readerNewDataAboutEmployees[2].ToString() + " " + readerNewDataAboutEmployees[3].ToString());

                                //if (readerNewDataAboutEmployees[1].ToString() == "Панков")
                                //{
                                    //defaultReasponsibleId = Convert.ToInt32(readerNewDataAboutEmployees[0]);
                                    //Console.WriteLine("Default reaponsible: " + defaultReasponsibleId.ToString() + " " + readerNewDataAboutEmployees[1].ToString() + " " + readerNewDataAboutEmployees[2].ToString() + " " + readerNewDataAboutEmployees[3].ToString());
                                //}
                            }
                            readerNewDataAboutEmployees.Close();

                            Console.WriteLine("Updating Houses data..");

                            String qsInsertHouses = "INSERT INTO dbo.Houses([EmployeeId],[NriId],[Region],[Filial],[CityId],[City],[AreaId],[Area],[StreetName],[StreetType],[Number1],[Type1],[Number2],[Type2],[Number3],[Type3],[GFK],[ERP],[RoYear],[Status],[IsPersonal],[FactTransferredToAgreement],[FactAgreed],[FactApproved],[UK],[UkContacts],[FactDocumentDate],[FactDocumentLink],[EcmDocumentId],[FactDocumentDescription],[Flats],[Floors],[Entrances],[PlanAgreementPrice],[PlanRentPrice],[NriRentPlan],[NriRentFact],[Updated],[HasContractAdvertising],[HasContractAdvancedAccess]) VALUES " +
                                "(@employeeId, @nriId, @region, @filial, @cityId, @city, @areaId, @area, @streetName, @streetType, @number1, @type1, @number2, @type2, @number3, @type3, @gFK, @eRP, @roYear, @status, @isPersonal, @factTransferredToAgreement, @factAgreed, @factApproved, @uK, @ukContacts, @factDocumentDate, @factDocumentLink, @ecmDocumentId, @factDocumentDescription, @flats, @floors, @entrances, @planAgreementPrice, @planRentPrice, @nriRentPlan, @nriRentFact, @updated, @hasContractAdvertising, @hasContractAdvancedAccess)";
                            String qsUpdateHouses = "UPDATE dbo.Houses SET EmployeeId = @employeeId, Region = @region, Filial = @filial, CityId = @cityId, City = @city, AreaId = @areaId " +
                                ", Area = @area, StreetName = @streetName, StreetType = @streetType, Number1 = @number1, Type1 = @type1" +
                                ", Number2 = @number2, Type2 = @type2, Number3 = @number3, Type3 = @type3, GFK = @gFK" +
                                ", ERP = @eRP, RoYear = @roYear, Status = @status, FactTransferredToAgreement = @factTransferredToAgreement, FactAgreed = @factAgreed, FactApproved = @factApproved, UK = @uK, UkContacts = @ukContacts, FactDocumentDate = @factDocumentDate, FactDocumentLink = @factDocumentLink, EcmDocumentId = @ecmDocumentId, FactDocumentDescription = @factDocumentDescription, Flats = @flats, Floors = @floors, Entrances = @entrances, PlanAgreementPrice = @planAgreementPrice, PlanRentPrice = @planRentPrice, NriRentPlan = @nriRentPlan, NriRentFact = @nriRentFact, Updated = @updated WHERE NriId = @nriId";

                            double percentCalc = Double.NaN; percentCalc = 0;
                            int percentTotal = HousesDataList.Count;
                            int percentCurr = 0; 

                            foreach (string currHouse in HousesDataList)
                            {
                                string hId = currHouse.Split(';')[0];
                                int CurrentHouseId = -1;
                                if (Int32.TryParse(hId, out CurrentHouseId) && currHouse.Split(';').Count() >= 68)
                                {
                                    string hResponsible = currHouse.Split(';')[65];
                                    if (HouseSqlIds.IndexOf(hId) > -1)
                                    {
                                        //UPDATE
                                        SqlCommand updCommand = new SqlCommand(qsUpdateHouses, connection);
                                        updCommand.Parameters.AddWithValue("@nriId", CurrentHouseId);

                                        int? ResponsibleId = null;
                                        if (EmployeeDbFIO.IndexOf(hResponsible) > -1)
                                        {
                                            ResponsibleId = EmployeeDbIds[EmployeeDbFIO.IndexOf(hResponsible)];
                                            updCommand.Parameters.AddWithValue("@employeeId", ResponsibleId);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@employeeId", DBNull.Value);
                                        }

                                        updCommand.Parameters.AddWithValue("@region", currHouse.Split(';')[3]);
                                        updCommand.Parameters.AddWithValue("@filial", currHouse.Split(';')[4]);
                                        updCommand.Parameters.AddWithValue("@cityId", Convert.ToInt32(currHouse.Split(';')[5]));
                                        updCommand.Parameters.AddWithValue("@city", currHouse.Split(';')[6]);
                                        updCommand.Parameters.AddWithValue("@areaId", Convert.ToInt32(currHouse.Split(';')[7]));
                                        updCommand.Parameters.AddWithValue("@area", currHouse.Split(';')[8]);
                                        updCommand.Parameters.AddWithValue("@streetName", currHouse.Split(';')[9]);
                                        updCommand.Parameters.AddWithValue("@streetType", currHouse.Split(';')[10]);
                                        updCommand.Parameters.AddWithValue("@number1", currHouse.Split(';')[11]);
                                        updCommand.Parameters.AddWithValue("@type1", currHouse.Split(';')[12]);
                                        updCommand.Parameters.AddWithValue("@number2", currHouse.Split(';')[13]);
                                        updCommand.Parameters.AddWithValue("@type2", currHouse.Split(';')[14]);
                                        updCommand.Parameters.AddWithValue("@number3", currHouse.Split(';')[15]);
                                        updCommand.Parameters.AddWithValue("@type3", currHouse.Split(';')[16]);
                                        updCommand.Parameters.AddWithValue("@gFK", currHouse.Split(';')[17]);
                                        updCommand.Parameters.AddWithValue("@eRP", currHouse.Split(';')[18]);
                                        int activationDate = (!String.IsNullOrEmpty(currHouse.Split(';')[63]) && (currHouse.Split(';')[63]).Replace('/', '.').Split('.').Count() == 3) ? Convert.ToInt32(((currHouse.Split(';')[63]).Split(' ')[0].Replace('/', '.').Split('.')[2])) : -1;
                                        updCommand.Parameters.AddWithValue("@roYear", activationDate);
                                        updCommand.Parameters.AddWithValue("@status", currHouse.Split(';')[19]);
                                        updCommand.Parameters.AddWithValue("@uK", currHouse.Split(';')[66]);
                                        updCommand.Parameters.AddWithValue("@ukContacts", currHouse.Split(';')[67]);

                                        DateTime NriRentPlan = new DateTime();
                                        if (DateTime.TryParse(currHouse.Split(';')[96], out NriRentPlan))
                                        {
                                            updCommand.Parameters.AddWithValue("@nriRentPlan", NriRentPlan);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@nriRentPlan", DBNull.Value);
                                        }

                                        DateTime NriRentFact = new DateTime();
                                        if (DateTime.TryParse(currHouse.Split(';')[97], out NriRentFact))
                                        {
                                            updCommand.Parameters.AddWithValue("@nriRentFact", NriRentFact);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@nriRentFact", DBNull.Value);
                                        }
                                        updCommand.Parameters.AddWithValue("@updated", HousesDate);

                                        int flts = 0;
                                        if (int.TryParse(currHouse.Split(';')[26], out flts))
                                        {
                                            updCommand.Parameters.AddWithValue("@flats", flts);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@flats", DBNull.Value);
                                        }
                                        int flrs = 0;
                                        if (int.TryParse(currHouse.Split(';')[25], out flrs))
                                        {
                                            updCommand.Parameters.AddWithValue("@floors", flrs);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@floors", DBNull.Value);
                                        }
                                        int entr = 0;
                                        if (int.TryParse(currHouse.Split(';')[24], out entr))
                                        {
                                            updCommand.Parameters.AddWithValue("@entrances", entr);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@entrances", DBNull.Value);
                                        }
                                        decimal agr = 0;
                                        if (decimal.TryParse(currHouse.Split(';')[29], out agr))
                                        {
                                            updCommand.Parameters.AddWithValue("@planAgreementPrice", agr);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@planAgreementPrice", DBNull.Value);
                                        }
                                        decimal rent = 0;
                                        if (decimal.TryParse(currHouse.Split(';')[30], out rent))
                                        {
                                            updCommand.Parameters.AddWithValue("@planRentPrice", rent);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@planRentPrice", DBNull.Value);
                                        }


                                        if (DocIds.IndexOf(CurrentHouseId) > -1)
                                        {
                                            Doc cDoc = Docs.Where(x => x.hId == CurrentHouseId).OrderByDescending(x => x.CreationDate).FirstOrDefault();
                                            updCommand.Parameters.AddWithValue("@factDocumentDate", cDoc.CreationDate);
                                            updCommand.Parameters.AddWithValue("@factDocumentLink", cDoc.Link);
                                            updCommand.Parameters.AddWithValue("@ecmDocumentId", cDoc.DocId);
                                            updCommand.Parameters.AddWithValue("@factDocumentDescription", cDoc.Description);
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@factDocumentDate", DBNull.Value);
                                            updCommand.Parameters.AddWithValue("@factDocumentLink", DBNull.Value);
                                            updCommand.Parameters.AddWithValue("@ecmDocumentId", DBNull.Value);
                                            updCommand.Parameters.AddWithValue("@factDocumentDescription", DBNull.Value);
                                        }

                                        int lInd = LogsIds.IndexOf(CurrentHouseId);
                                        if (lInd > -1)
                                        {
                                            if (Logs[lInd].GetHouseDate != null)
                                            {
                                                updCommand.Parameters.AddWithValue("@factTransferredToAgreement", Logs[lInd].GetHouseDate);
                                            }
                                            else
                                            {
                                                updCommand.Parameters.AddWithValue("@factTransferredToAgreement", DBNull.Value);
                                            }
                                            if (Logs[lInd].FactAgreed != null)
                                            {
                                                updCommand.Parameters.AddWithValue("@factAgreed", Logs[lInd].FactAgreed);
                                            }
                                            else
                                            {
                                                updCommand.Parameters.AddWithValue("@factAgreed", DBNull.Value);
                                            }
                                            if (Logs[lInd].FactApproved != null)
                                            {
                                                updCommand.Parameters.AddWithValue("@factApproved", Logs[lInd].FactApproved);
                                            }
                                            else
                                            {
                                                updCommand.Parameters.AddWithValue("@factApproved", DBNull.Value);
                                            }
                                        }
                                        else
                                        {
                                            updCommand.Parameters.AddWithValue("@factTransferredToAgreement", DBNull.Value);
                                            updCommand.Parameters.AddWithValue("@factAgreed", DBNull.Value);
                                            updCommand.Parameters.AddWithValue("@factApproved", DBNull.Value);
                                        }

                                        updCommand.ExecuteNonQuery();
                                        UpdatedHouses += 1;
                                    }
                                    else
                                    {
                                        //INSERT
                                        SqlCommand insCommand = new SqlCommand(qsInsertHouses, connection);
                                        insCommand.Parameters.AddWithValue("@nriId", CurrentHouseId);

                                        int? ResponsibleId = null;
                                        if (EmployeeDbFIO.IndexOf(hResponsible) > -1)
                                        {
                                            ResponsibleId = EmployeeDbIds[EmployeeDbFIO.IndexOf(hResponsible)];
                                            insCommand.Parameters.AddWithValue("@employeeId", ResponsibleId);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@employeeId", DBNull.Value);
                                        }

                                        insCommand.Parameters.AddWithValue("@region", currHouse.Split(';')[3]);
                                        insCommand.Parameters.AddWithValue("@filial", currHouse.Split(';')[4]);
                                        insCommand.Parameters.AddWithValue("@cityId", Convert.ToInt32(currHouse.Split(';')[5]));
                                        insCommand.Parameters.AddWithValue("@city", currHouse.Split(';')[6]);
                                        insCommand.Parameters.AddWithValue("@areaId", Convert.ToInt32(currHouse.Split(';')[7]));
                                        insCommand.Parameters.AddWithValue("@area", currHouse.Split(';')[8]);
                                        insCommand.Parameters.AddWithValue("@streetName", currHouse.Split(';')[9]);
                                        insCommand.Parameters.AddWithValue("@streetType", currHouse.Split(';')[10]);
                                        insCommand.Parameters.AddWithValue("@number1", currHouse.Split(';')[11]);
                                        insCommand.Parameters.AddWithValue("@type1", currHouse.Split(';')[12]);
                                        insCommand.Parameters.AddWithValue("@number2", currHouse.Split(';')[13]);
                                        insCommand.Parameters.AddWithValue("@type2", currHouse.Split(';')[14]);
                                        insCommand.Parameters.AddWithValue("@number3", currHouse.Split(';')[15]);
                                        insCommand.Parameters.AddWithValue("@type3", currHouse.Split(';')[16]);
                                        insCommand.Parameters.AddWithValue("@gFK", currHouse.Split(';')[17]);
                                        insCommand.Parameters.AddWithValue("@eRP", currHouse.Split(';')[18]);
                                        int activationDate = (!String.IsNullOrEmpty(currHouse.Split(';')[63]) && (currHouse.Split(';')[63]).Replace('/', '.').Split('.').Count() == 3) ? Convert.ToInt32(((currHouse.Split(';')[63]).Split(' ')[0].Replace('/', '.').Split('.')[2])) : -1;
                                        insCommand.Parameters.AddWithValue("@roYear", activationDate);
                                        insCommand.Parameters.AddWithValue("@status", currHouse.Split(';')[19]);
                                        insCommand.Parameters.AddWithValue("@uK", currHouse.Split(';')[66]);
                                        insCommand.Parameters.AddWithValue("@ukContacts", currHouse.Split(';')[67]);
                                        insCommand.Parameters.AddWithValue("@isPersonal", false);
                                        insCommand.Parameters.AddWithValue("@hasContractAdvertising", false);
                                        insCommand.Parameters.AddWithValue("@hasContractAdvancedAccess", false);


                                        DateTime NriRentPlan = new DateTime();
                                        if (DateTime.TryParse(currHouse.Split(';')[96], out NriRentPlan))
                                        {
                                            insCommand.Parameters.AddWithValue("@nriRentPlan", NriRentPlan);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@nriRentPlan", DBNull.Value);
                                        }

                                        DateTime NriRentFact = new DateTime();
                                        if (DateTime.TryParse(currHouse.Split(';')[97], out NriRentFact))
                                        {
                                            insCommand.Parameters.AddWithValue("@nriRentFact", NriRentFact);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@nriRentFact", DBNull.Value);
                                        }
                                        insCommand.Parameters.AddWithValue("@updated", HousesDate);

                                        int flts = 0;
                                        if (int.TryParse(currHouse.Split(';')[26], out flts))
                                        {
                                            insCommand.Parameters.AddWithValue("@flats", flts);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@flats", DBNull.Value);
                                        }
                                        int flrs = 0;
                                        if (int.TryParse(currHouse.Split(';')[25], out flrs))
                                        {
                                            insCommand.Parameters.AddWithValue("@floors", flrs);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@floors", DBNull.Value);
                                        }
                                        int entr = 0;
                                        if (int.TryParse(currHouse.Split(';')[24], out entr))
                                        {
                                            insCommand.Parameters.AddWithValue("@entrances", entr);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@entrances", DBNull.Value);
                                        }
                                        decimal agr = 0;
                                        if (decimal.TryParse(currHouse.Split(';')[29], out agr))
                                        {
                                            insCommand.Parameters.AddWithValue("@planAgreementPrice", agr);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@planAgreementPrice", DBNull.Value);
                                        }
                                        decimal rent = 0;
                                        if (decimal.TryParse(currHouse.Split(';')[30], out rent))
                                        {
                                            insCommand.Parameters.AddWithValue("@planRentPrice", rent);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@planRentPrice", DBNull.Value);
                                        }

                                        if (DocIds.IndexOf(CurrentHouseId) > -1)
                                        {
                                            Doc cDoc = Docs.Where(x => x.hId == CurrentHouseId).OrderByDescending(x => x.CreationDate).FirstOrDefault();
                                            insCommand.Parameters.AddWithValue("@factDocumentDate", cDoc.CreationDate);
                                            insCommand.Parameters.AddWithValue("@factDocumentLink", cDoc.Link);
                                            insCommand.Parameters.AddWithValue("@ecmDocumentId", cDoc.DocId);
                                            insCommand.Parameters.AddWithValue("@factDocumentDescription", cDoc.Description);
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@factDocumentDate", DBNull.Value);
                                            insCommand.Parameters.AddWithValue("@factDocumentLink", DBNull.Value);
                                            insCommand.Parameters.AddWithValue("@ecmDocumentId", DBNull.Value);
                                            insCommand.Parameters.AddWithValue("@factDocumentDescription", DBNull.Value);
                                        }

                                        int lInd = LogsIds.IndexOf(CurrentHouseId);
                                        if (lInd > -1)
                                        {
                                            if (Logs[lInd].GetHouseDate != null)
                                            {
                                                insCommand.Parameters.AddWithValue("@factTransferredToAgreement", Logs[lInd].GetHouseDate);
                                            }
                                            else
                                            {
                                                insCommand.Parameters.AddWithValue("@factTransferredToAgreement", DBNull.Value);
                                            }

                                            if (Logs[lInd].FactAgreed != null)
                                            {
                                                insCommand.Parameters.AddWithValue("@factAgreed", Logs[lInd].FactAgreed);
                                            }
                                            else
                                            {
                                                insCommand.Parameters.AddWithValue("@factAgreed", DBNull.Value);
                                            }

                                            if(Logs[lInd].FactApproved != null)
                                            {
                                                insCommand.Parameters.AddWithValue("@factApproved", Logs[lInd].FactApproved);
                                            }
                                            else
                                            {
                                                insCommand.Parameters.AddWithValue("@factApproved", DBNull.Value);
                                            }
                                        }
                                        else
                                        {
                                            insCommand.Parameters.AddWithValue("@factTransferredToAgreement", DBNull.Value);
                                            insCommand.Parameters.AddWithValue("@factAgreed", DBNull.Value);
                                            insCommand.Parameters.AddWithValue("@factApproved", DBNull.Value);
                                        }

                                        insCommand.ExecuteNonQuery();
                                        InsertedHouses += 1;
                                    }
                                }
                                else
                                {
                                    BadHouses += 1;
                                    Console.WriteLine("Bad house: " + currHouse);
                                }

                                percentCurr++;
                                if (percentCurr % 100 == 0)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Updating Houses data..");
                                    percentCalc = Double.Parse(percentCurr.ToString()) / percentTotal * 100.0;
                                    Console.WriteLine(percentCalc.ToString().Split(',')[0].Split('.')[0] + "% done");
                                }
                            }

                            Console.Clear();
                            Console.WriteLine("Updating Management companies:");
                            Console.WriteLine("Starting...");

                            List<ManagementCompany> nManageComp = new List<ManagementCompany>();

                            string qsManagementCompanies = "SELECT [Region], [Filial], [CityId], [City], [UK], COUNT(case [RoYear] when 2018 then 1 else null end), COUNT(case [RoYear] when 2019 then 1 else null end) FROM [dbo].[Houses] GROUP BY [Region], [Filial], [CityId], [City], [UK]";
                            SqlCommand cmdSelectManagementCompanies = new SqlCommand(qsManagementCompanies, connection);

                            SqlDataReader readerManagementCompanies = cmdSelectManagementCompanies.ExecuteReader();
                            while (readerManagementCompanies.Read())
                            {
                                ManagementCompany nMC = new ManagementCompany();
                                nMC.Region = readerManagementCompanies[0].ToString();
                                nMC.Filial = readerManagementCompanies[1].ToString();
                                nMC.CityId = Convert.ToInt32(readerManagementCompanies[2].ToString());
                                nMC.City = readerManagementCompanies[3].ToString();
                                nMC.Name = readerManagementCompanies[4].ToString();
                                nMC.RO18Houses = Convert.ToInt32(readerManagementCompanies[5].ToString());
                                nMC.RO19Houses = Convert.ToInt32(readerManagementCompanies[6].ToString());
                                nManageComp.Add(nMC);                            
                            }
                            readerManagementCompanies.Close();

                            List<ManagementCompany> oldManageComp = new List<ManagementCompany>();

                            string qsOldManagementCompanies = "SELECT [Id], [Region], [Filial], [CityId], [City], [Name], [Contacts], [RO18Houses], [RO19Houses] FROM [dbo].[ManagementCompanys]";
                            SqlCommand cmdSelectOldManagementCompanies = new SqlCommand(qsOldManagementCompanies, connection);

                            SqlDataReader readerOldManagementCompanies = cmdSelectOldManagementCompanies.ExecuteReader();
                            while (readerOldManagementCompanies.Read())
                            {
                                ManagementCompany oMC = new ManagementCompany();
                                oMC.Id = Convert.ToInt32(readerOldManagementCompanies[0].ToString());
                                oMC.Region = readerOldManagementCompanies[1].ToString();
                                oMC.Filial = readerOldManagementCompanies[2].ToString();
                                oMC.CityId = Convert.ToInt32(readerOldManagementCompanies[3].ToString());
                                oMC.City = readerOldManagementCompanies[4].ToString();
                                oMC.Name = readerOldManagementCompanies[5].ToString();
                                oMC.Contacts = readerOldManagementCompanies[6].ToString();
                                oMC.RO18Houses = Convert.ToInt32(readerOldManagementCompanies[7].ToString());
                                oMC.RO19Houses = Convert.ToInt32(readerOldManagementCompanies[8].ToString());
                                oldManageComp.Add(oMC);                                
                            }
                            readerOldManagementCompanies.Close();


                            Console.Clear();
                            Console.WriteLine("Updating Management companies:");
                            Console.WriteLine("In progress...");

                            int insertedManageCompanies = 0;
                            int updatedManageCompanies = 0;

                            foreach (var newComp in nManageComp)
                            {
                                if (oldManageComp.Where(x => x.Region == newComp.Region && x.Filial == newComp.Filial && x.CityId == newComp.CityId && x.City == newComp.City && x.Name == newComp.Name).Count() == 0)
                                {
                                    String qsInsertManagementCompanies = "INSERT INTO dbo.ManagementCompanys([Region],[Filial],[CityId],[City],[Name],[Contacts],[RO18Houses],[RO19Houses]) VALUES " +
                                "(@region, @filial, @cityId, @city, @name, @contacts, @ro18houses, @ro19houses)";
                                    SqlCommand insManagementCompanyCommand = new SqlCommand(qsInsertManagementCompanies, connection);
                                    insManagementCompanyCommand.Parameters.AddWithValue("@region", newComp.Region);
                                    insManagementCompanyCommand.Parameters.AddWithValue("@filial", newComp.Filial);
                                    insManagementCompanyCommand.Parameters.AddWithValue("@cityId", newComp.CityId);
                                    insManagementCompanyCommand.Parameters.AddWithValue("@city", newComp.City);
                                    insManagementCompanyCommand.Parameters.AddWithValue("@name", newComp.Name);
                                    insManagementCompanyCommand.Parameters.AddWithValue("@contacts", "to be developed");
                                    insManagementCompanyCommand.Parameters.AddWithValue("@ro18houses", newComp.RO18Houses);
                                    insManagementCompanyCommand.Parameters.AddWithValue("@ro19houses", newComp.RO19Houses);
                                    insManagementCompanyCommand.ExecuteNonQuery();

                                    insertedManageCompanies++;
                                }
                                else
                                {
                                    int oldCompId = oldManageComp.Where(x => x.Region == newComp.Region && x.Filial == newComp.Filial && x.CityId == newComp.CityId && x.City == newComp.City && x.Name == newComp.Name).FirstOrDefault().Id;

                                    String qsUpdateManagementCompanies = "UPDATE dbo.ManagementCompanys SET Region = @region, Filial = @filial, CityId = @cityId, City = @city" +
                                    ", Name = @name, Contacts = @contacts, RO18Houses = @ro18houses, RO19Houses = @ro19houses WHERE Id = @oid";
                                    SqlCommand updManagementCompanyCommand = new SqlCommand(qsUpdateManagementCompanies, connection);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@oid", oldCompId);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@region", newComp.Region);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@filial", newComp.Filial);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@cityId", newComp.CityId);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@city", newComp.City);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@name", newComp.Name);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@contacts", "to be developed");
                                    updManagementCompanyCommand.Parameters.AddWithValue("@ro18houses", newComp.RO18Houses);
                                    updManagementCompanyCommand.Parameters.AddWithValue("@ro19houses", newComp.RO19Houses);
                                    updManagementCompanyCommand.ExecuteNonQuery();

                                    oldManageComp = oldManageComp.Where(x => x.Id != oldCompId).ToList(); //жопа с производительностью

                                    updatedManageCompanies++;
                                }
                            }

                            int nulledManageComp = 0;
                            foreach (var comp in oldManageComp)
                            {
                                String qsUpdateLastManagementCompanies = "UPDATE dbo.ManagementCompanys SET RO18Houses = @ro18houses, RO19Houses = @ro19houses WHERE Id = @cid";
                                SqlCommand updLastManagementCompanyCommand = new SqlCommand(qsUpdateLastManagementCompanies, connection);
                                updLastManagementCompanyCommand.Parameters.AddWithValue("@cid", comp.Id);
                                updLastManagementCompanyCommand.Parameters.AddWithValue("@ro18houses", 0);
                                updLastManagementCompanyCommand.Parameters.AddWithValue("@ro19houses", 0);
                                updLastManagementCompanyCommand.ExecuteNonQuery();

                                nulledManageComp++;
                            }

                            Console.Clear();

                            Console.WriteLine("Searching EcmContracts to update from SQL...");
                            string qsEcmContracts = "SELECT Id FROM dbo.EcmDocuments";
                            SqlCommand cmdSelectCurrentEcmDocuments = new SqlCommand(qsEcmContracts, connection);

                            SqlDataReader readerEcmDocuments = cmdSelectCurrentEcmDocuments.ExecuteReader();
                            while (readerEcmDocuments.Read())
                            {
                                ContractsExistingDataIds.Add(readerEcmDocuments[0].ToString());
                            }
                            readerEcmDocuments.Close();

                            Console.WriteLine("Found " + ContractsExistingDataIds.Count.ToString() + " EcmContracts to update in SQL");

                            Console.WriteLine("Searching EcmContracts to update at houses from SQL...");
                            string qsEcmContractsAtHouses = "SELECT DISTINCT EcmDocumentId FROM dbo.Houses WHERE EcmDocumentId IS NOT NULL";
                            SqlCommand cmdSelectCurrentEcmDocumentsAtHouses = new SqlCommand(qsEcmContractsAtHouses, connection);

                            SqlDataReader readerEcmDocumentsAtHouses = cmdSelectCurrentEcmDocumentsAtHouses.ExecuteReader();
                            while (readerEcmDocumentsAtHouses.Read())
                            {
                                ECMContractsAtHouses.Add(readerEcmDocumentsAtHouses[0].ToString());
                            }
                            readerEcmDocumentsAtHouses.Close();

                            Console.WriteLine("Found " + ECMContractsAtHouses.Count.ToString() + " EcmContractsat houses to update in SQL");

                            if (ECMContractsAtHouses.Count > 0)
                            {
                                for (int c = 0; c < ECMContractsAtHouses.Count; c++)
                                {
                                    string hContractId = ECMContractsAtHouses[c];

                                    if (ContractsDataIds.IndexOf(hContractId) > -1)
                                    {
                                        string contractData = ContractsDataList[ContractsDataIds.IndexOf(hContractId)];

                                        string cStatus = contractData.Split('\t')[6];
                                        string cType = contractData.Split('\t')[8];
                                        string cLandlord = contractData.Split('\t')[12];
                                        string cINN = contractData.Split('\t')[13];
                                        string cNumberBase= contractData.Split('\t')[22];
                                        string cNumberAddon = contractData.Split('\t')[24];
                                        string cSevdId = contractData.Split('\t')[25];
                                        string cSevdLink = contractData.Split('\t')[26];
                                        DateTime cStartDate = DateTime.MinValue;
                                        DateTime.TryParse(contractData.Split('\t')[27], out cStartDate);
                                        DateTime cSignDate = DateTime.MinValue;
                                        DateTime.TryParse(contractData.Split('\t')[83], out cSignDate);
                                        decimal cPrice = Decimal.Zero;
                                        decimal.TryParse(contractData.Split('\t')[31].Replace(',', '.'), out cPrice);
                                        string cCurrency = contractData.Split('\t')[32];
                                        string cNDS = contractData.Split('\t')[39];
                                        int cZpNumber = 0;
                                        int.TryParse(contractData.Split('\t')[50], out cZpNumber);
                                        string cSystemNumber = contractData.Split('\t')[52];
                                        string cEcmComment = contractData.Split('\t')[64];
                                        string cZpNfsLink = contractData.Split('\t')[85];

                                        if (ContractsExistingDataIds.IndexOf(hContractId) > -1)
                                        {
                                            String qsUpdateEcmContractData = "UPDATE dbo.EcmDocuments SET Status = @status, Type = @type, Landlord = @landlord, INN = @inn, NumberBase = @numberBase, NumberAddon = @numberAddon, SevdId = @sevdId, SevdLink = @sevdLink, StartDate = @startDate, Price = @price, Currency = @currency, NDS = @nDS, ZpNumber = @zpNumber, SystemNumber = @systemNumber, EcmComment = @ecmComment, SignDate = @signDate, ZpNfsLink = @zpNfsLink WHERE Id = @cid";

                                            SqlCommand updEcmContractDataCommand = new SqlCommand(qsUpdateEcmContractData, connection);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@cid", hContractId);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@status", cStatus);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@type", cType);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@landlord", cLandlord);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@inn", cINN);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@numberBase", cNumberBase);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@numberAddon", cNumberAddon);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@sevdId", cSevdId);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@sevdLink", cSevdLink);
                                            if (cStartDate >= new DateTime(2000, 1, 1))
                                            {
                                                updEcmContractDataCommand.Parameters.AddWithValue("@startDate", cStartDate);
                                            }
                                            else
                                            {
                                                updEcmContractDataCommand.Parameters.AddWithValue("@startDate", DBNull.Value);
                                            }                                            
                                            updEcmContractDataCommand.Parameters.AddWithValue("@price", cPrice);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@currency", cCurrency);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@nDS", cNDS);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@zpNumber", cZpNumber);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@systemNumber", cSystemNumber);
                                            updEcmContractDataCommand.Parameters.AddWithValue("@ecmComment", cEcmComment);
                                            if (cSignDate >= new DateTime(2000, 1, 1))
                                            {
                                                updEcmContractDataCommand.Parameters.AddWithValue("@signDate", cSignDate);
                                            }
                                            else
                                            {
                                                updEcmContractDataCommand.Parameters.AddWithValue("@signDate", DBNull.Value);
                                            }                                            
                                            updEcmContractDataCommand.Parameters.AddWithValue("@zpNfsLink", cZpNfsLink);
                                            updEcmContractDataCommand.ExecuteNonQuery();
                                            UpdatedEcmDocuments++;
                                        }
                                        else
                                        {
                                            String qsInsertEcmContractData = "INSERT INTO dbo.EcmDocuments (Id, Status, Type, Landlord, INN, NumberBase, NumberAddon, SevdId, SevdLink, StartDate, Price, Currency, NDS, ZpNumber, SystemNumber, EcmComment, SignDate, ZpNfsLink) VALUES (@cid, @status, @type, @landlord, @inn, @numberBase, @numberAddon, @sevdId, @sevdLink, @startDate, @price, @currency, @nDS, @zpNumber, @systemNumber, @ecmComment, @signDate, @zpNfsLink)";
                                            SqlCommand insEcmContractDataCommand = new SqlCommand(qsInsertEcmContractData, connection);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@cid", hContractId);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@status", cStatus);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@type", cType);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@landlord", cLandlord);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@inn", cINN);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@numberBase", cNumberBase);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@numberAddon", cNumberAddon);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@sevdId", cSevdId);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@sevdLink", cSevdLink);
                                            if (cStartDate >= new DateTime(2000, 1, 1))
                                            {
                                                insEcmContractDataCommand.Parameters.AddWithValue("@startDate", cStartDate);
                                            }
                                            else
                                            {
                                                insEcmContractDataCommand.Parameters.AddWithValue("@startDate", DBNull.Value);
                                            }
                                            insEcmContractDataCommand.Parameters.AddWithValue("@price", cPrice);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@currency", cCurrency);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@nDS", cNDS);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@zpNumber", cZpNumber);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@systemNumber", cSystemNumber);
                                            insEcmContractDataCommand.Parameters.AddWithValue("@ecmComment", cEcmComment);
                                            if (cSignDate >= new DateTime(2000, 1, 1))
                                            {
                                                insEcmContractDataCommand.Parameters.AddWithValue("@signDate", cSignDate);
                                            }
                                            else
                                            {
                                                insEcmContractDataCommand.Parameters.AddWithValue("@signDate", DBNull.Value);
                                            }                                            
                                            insEcmContractDataCommand.Parameters.AddWithValue("@zpNfsLink", cZpNfsLink);
                                            insEcmContractDataCommand.ExecuteNonQuery();
                                            InsertedEcmDocuments++;
                                        }
                                    }
                                }
                            }

                            Console.Clear();

                            Console.WriteLine("All operations done SUCCESSFULL");
                            Console.WriteLine("Inserted Employees: " + InsertedEmployees.ToString());
                            Console.WriteLine("Updated Employees: " + UpdatedEmployees.ToString());
                            Console.WriteLine("Inserted Houses: " + InsertedHouses.ToString());
                            Console.WriteLine("Updated Houses: " + UpdatedHouses.ToString());
                            Console.WriteLine("Bad Houses: " + BadHouses.ToString());
                            Console.WriteLine("Inserted ManageCompanies: " + insertedManageCompanies.ToString());
                            Console.WriteLine("Updated ManageCompanies: " + updatedManageCompanies.ToString());
                            Console.WriteLine("Nulled ManageCompanies: " + nulledManageComp.ToString());
                            Console.WriteLine("Inserted EcmDocuments: " + InsertedEcmDocuments.ToString());
                            Console.WriteLine("Updated EcmDocuments: " + UpdatedEcmDocuments.ToString());
                        }
                        catch (Exception excp)
                        {
                            Console.WriteLine("Writing data: UNSUCCESS");
                            Console.WriteLine(excp.ToString());
                        }
                        finally
                        {
                            String qsInsertDatabaseState = "INSERT INTO dbo.DatabaseStates([State],[Progress],[Success],[SourceDate]) VALUES (@state, @progress, @success, @sourceDate)";

                            SqlCommand insDatabaseStateCommand = new SqlCommand(qsInsertDatabaseState, connection);

                            //Houses
                            insDatabaseStateCommand.Parameters.AddWithValue("@state", "NriHouses");
                            insDatabaseStateCommand.Parameters.AddWithValue("@progress", 100);
                            insDatabaseStateCommand.Parameters.AddWithValue("@success", 1);
                            insDatabaseStateCommand.Parameters.AddWithValue("@sourceDate", HousesDate);
                            insDatabaseStateCommand.ExecuteNonQuery();

                            //Logs
                            insDatabaseStateCommand.Parameters.Clear();
                            insDatabaseStateCommand.Parameters.AddWithValue("@state", "NriLogs");
                            insDatabaseStateCommand.Parameters.AddWithValue("@progress", 100);
                            insDatabaseStateCommand.Parameters.AddWithValue("@success", 1);
                            insDatabaseStateCommand.Parameters.AddWithValue("@sourceDate", LogsDate);
                            insDatabaseStateCommand.ExecuteNonQuery();

                            //Employees
                            insDatabaseStateCommand.Parameters.Clear();
                            insDatabaseStateCommand.Parameters.AddWithValue("@state", "NriEmployees");
                            insDatabaseStateCommand.Parameters.AddWithValue("@progress", 100);
                            insDatabaseStateCommand.Parameters.AddWithValue("@success", 1);
                            insDatabaseStateCommand.Parameters.AddWithValue("@sourceDate", EmployeesDate);
                            insDatabaseStateCommand.ExecuteNonQuery();

                            //Docs
                            insDatabaseStateCommand.Parameters.Clear();
                            insDatabaseStateCommand.Parameters.AddWithValue("@state", "NriDocuments");
                            insDatabaseStateCommand.Parameters.AddWithValue("@progress", 100);
                            insDatabaseStateCommand.Parameters.AddWithValue("@success", 1);
                            insDatabaseStateCommand.Parameters.AddWithValue("@sourceDate", DocsDate);
                            insDatabaseStateCommand.ExecuteNonQuery();

                            //ECM
                            insDatabaseStateCommand.Parameters.Clear();
                            insDatabaseStateCommand.Parameters.AddWithValue("@state", "EcmDocuments");
                            insDatabaseStateCommand.Parameters.AddWithValue("@progress", 100);
                            insDatabaseStateCommand.Parameters.AddWithValue("@success", 1);
                            insDatabaseStateCommand.Parameters.AddWithValue("@sourceDate", EcmDocumentsDate);
                            insDatabaseStateCommand.ExecuteNonQuery();

                            //Arfs
                            insDatabaseStateCommand.Parameters.Clear();
                            insDatabaseStateCommand.Parameters.AddWithValue("@state", "Arfs");
                            insDatabaseStateCommand.Parameters.AddWithValue("@progress", 100);
                            insDatabaseStateCommand.Parameters.AddWithValue("@success", 1);
                            insDatabaseStateCommand.Parameters.AddWithValue("@sourceDate", ArfsDate);
                            insDatabaseStateCommand.ExecuteNonQuery();

                            connection.Close();
                        }

                    }                    
                    //Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Data tests NOT PASSED. Please check data files.");
                    //Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Information tests NOT PASSED. Please check files and connection to SQL Server.");
                //Console.ReadLine();
            }
        }
    }
}
